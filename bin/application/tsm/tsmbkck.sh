#!/bin/sh
#Load the tsm backup log
#5 5 * * * /mn/bin/application/tsm/tsmbkck.sh -1
#5 2,4,6,8,10,12,14,16,18,20,22,23 * * *  /mn/bin/application/tsm/tsmbkck.sh
scriptsdir=$(cd `dirname $0`; pwd)/../../..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
datafiletmp=${scriptsdir}/data/app.tsmchktmp.dat
linedata=""
>$datafiletmp
export NLS_LANG=american_america.zhs16gbk
IFSBK=$IFS 
IFS='
'
str=""
if [[ $1 -lt 0 ]];then
	str="begind=$1"
fi
/usr/bin/dsmadmc -DISPLaymode=LISt -DATAOnly=yes  -id=admin -password=admin  "query even * * $str" |while read line
do
	if [[ $line == "" ]];then		
		echo $linedata>>$datafiletmp
		linedata=""
	else	
		linedata="$linedata $line"
	fi
done
IFS=$IFSBK

if [[ $dataformat -eq 1 ]];then
	while read LINE
	do
	# if [[ $(echo $LINE|grep -E "已完成|Completed"|wc -l) -eq 0 ]];then
	# 	#echo "insert into mn_node_log values('`hostname`$rid','backup_tsm',3,'$LINE','',sysdate)  "
	# 	echo "insert into mn_sys_bkrec_dtl_source(type,a,b,c,inserttime) values('`hostname`$rid','backup_tsm',3,'$LINE','',sysdate)">>$dataformaturl
	# 	echo "--NEXT" >>$dataformaturl
	# else
	# 	#echo "insert into mn_node_log values('`hostname`$rid','backup_tsm',0,'$LINE','',sysdate) "
	# 	echo "insert into mn_sys_bkrec_dtl_source values('`hostname`$rid','backup_tsm',0,'$LINE','',sysdate)">>$dataformaturl
	# 	echo "--NEXT" >>$dataformaturl
	# fi
	calname=`echo $LINE|awk -F "调度名称" '{print $2}'|awk -F "节点名" '{print $1}' |awk -F ":" {'print $2'}`
	echo "insert into mn_sys_bkrec_dtl_source(e,f,seq,source,type,a,inserttime) values('$calname','notprocess',seqbkrec_dtl_source.NextVal,'`hostname`$rid','tsm','$LINE',to_date(to_char(sysdate,'yyy-mm-dd hh24:mi'),'yyy-mm-dd hh24:mi'))"
	echo "insert into mn_sys_bkrec_dtl_source(e,f,seq,source,type,a,inserttime) values('$calname','notprocess',seqbkrec_dtl_source.NextVal,'`hostname`$rid','tsm','$LINE',to_date(to_char(sysdate,'yyy-mm-dd hh24:mi'),'yyy-mm-dd hh24:mi'))">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	done <$datafiletmp
fi
exit 0
 