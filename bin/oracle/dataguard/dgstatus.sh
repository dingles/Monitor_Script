#!/bin/sh
#
#check the dg apply status.
#/mon/mon/chkdgstatus.sh 

#############################################################
######加入到grid control 自定义度量中
###!/usr/bin/perl
###chkdgstatus.perl  
###
##my $res = `/usr/bin/sh /mon/mon/chkdgstatus.sh`; 
##print $res;
##exit 0;
#/usr/bin/perl /mon/mon/chkdgstatus.perl  
#
#diff_seq 
#  if standby db error,1000
#  if standby delay ,delay number
#############################################################
primary_user=mon
primary_pwd=oracle 
primary_tns=pos
export ORACLE_SID=pos
export ORACLE_BASE=/oracle
export ORACLE_HOME=$ORACLE_BASE/product/10.2.0/db_1
export PATH=$PATH:$ORACLE_HOME/bin:$ORACLE_HOME/OPatch:.:/usr/local/bin
Sspool_file=/tmp/standby_$$.dat
Pspool_file=/tmp/primary_$$.dat 
SQLPLUS=$ORACLE_HOME/bin/sqlplus


##################get standby applied seq
$SQLPLUS "/ as sysdba"   >/dev/null <<EOF
set termout off heading off echo off time off timing off feedback off
spool $Sspool_file
SELECT 'SEQ' ,THREAD#, MAX(SEQUENCE#) AS "LAST_APPLIED_LOG" FROM V\$LOG_HISTORY GROUP BY THREAD#;
quit
EOF
applied_seq=`cat $Sspool_file |grep -v SELECT|grep -v quit|grep SEQ|awk   '{print $3}'`
if [[ $applied_seq -eq "" ]] then
	echo "em_result=1000\n"
	rm -f $Sspool_file  >/dev/null 
	exit 1
fi


##################get primary now diffseq
$SQLPLUS "${primary_user}/${primary_pwd}@${primary_tns}"  >/dev/null  <<EOF
set termout off heading off echo off time off timing off feedback off
spool $Pspool_file
SELECT  'DIFF_SEQ' ,SEQUENCE#-$applied_seq,STATUS FROM V\$LOG WHERE STATUS='CURRENT';
quit
EOF
diff_seq=`cat $Pspool_file |grep -v SELECT|grep -v quit|grep SEQ|awk   '{print $2}'`

echo "em_result=${diff_seq}\n"   

rm -f $Sspool_file  >/dev/null 
rm -f $Pspool_file  >/dev/null 

exit 0

 