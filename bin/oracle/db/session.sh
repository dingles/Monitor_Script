#!/bin/sh
#mn.cnf:dataformat,dataformaturl
#sh run.sh $sid
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../../..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
#. ${scriptsdir}/lib/sysf/reclog.fun
if [[ -n $1 ]];then
	export ORACLE_SID=$1
fi
#env
spoolfile=${scriptsdir}/tmp/ora.db.session.$$.out
lockurl=${scriptsdir}/locks/ora.db.session.lck
if [[ `islock $lockurl` -eq 1 ]];then
	#reclog "ORADB.SESSION" "ERROR:file is locked."
    exit 1
fi
lock $lockurl $$
######Main############################################
sqlplus "/ as sysdba">/dev/null<<EOF
set embedded off
ttitle off
btitle off
set pagesize 0
set wrap off
set linesize 2000
set trimspool on
set newpage 0
set space 0
set trunc off
set arraysize 1
set heading off
set feedback off
set verify off
set termout off
set underline '-'
set pause off
clear breaks
clear columns
col msg for a90
spool $spoolfile
select 'DATA,'||(select count(1) from v\$session)||','||
       (select count(1) from v\$session where status = 'ACTIVE') msg
from dual;
spool off
quit
EOF
##format sqlfile
grep "DATA" $spoolfile|grep -v "select"|awk -F ',' '{print $2,$3}'|
while read f2 f3
do
if [[ $dataformat -eq 1 ]];then
    echo "insert into mn_oradb_session values('$rid.db.{$ORACLE_SID}','$ORACLE_SID',$f2,$f3,'$(date "+%Y%m%d:%H%M%S")',sysdate)">>$dataformaturl
    echo "--NEXT">>$dataformaturl
fi
done
######Exit############################################
rm -f $spoolfile>/dev/null
unlock $lockurl
exit 0