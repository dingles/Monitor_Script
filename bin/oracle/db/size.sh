#!/bin/sh
#mn.cnf:dataformat,dataformaturl
#sh run.sh $sid
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../../..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
#. ${scriptsdir}/lib/sysf/reclog.fun
if [[ -n $1 ]];then
  export ORACLE_SID=$1
fi
#env
spoolfile=$scriptsdir/tmp/ora.db.size.$$.out
lockurl=${scriptsdir}/locks/ora.db.size.lck
if [[ `islock $lockurl` -eq 1 ]];then
    #reclog "ORADB.SIZE" "ERROR:file is locked."
    exit 1
fi
lock $lockurl $$
######Main############################################
sqlplus "/ as sysdba">/dev/null<<EOF
set embedded off
ttitle off
btitle off
set pagesize 0
set wrap off
set linesize 2000
set trimspool on
set newpage 0
set space 0
set trunc off
set arraysize 1
set heading off
set feedback off
set verify off
set termout off
set underline '-'
set pause off
clear breaks
clear columns
col message for a90
spool $spoolfile
select  'DATA,'||a.space_allocated_m || ',' ||
       (a.space_allocated_m - b.space_free_m) message
  from (select trunc(sum(bytes) / 1024 / 1024, 2) space_allocated_m
          from v\$datafile) a,
       (select trunc(sum(bytes) / 1024 / 1024, 2) space_free_m
          from dba_free_space) b;
spool off
quit
EOF
##format sqlfile
grep "DATA" $spoolfile|grep -v "select"|awk -F ',' '{print $2,$3}'|
while read f2 f3
do
if [[ $dataformat -eq 1 ]];then
    echo "insert into mn_oradb_size values('$rid.db.{$ORACLE_SID}','$ORACLE_SID',$f2,$f3,'$(date "+%Y%m%d:%H%M%S")',sysdate)">>$dataformaturl
    echo "--NEXT">>$dataformaturl
fi
done
######Exit############################################
rm -f $spoolfile>/dev/null
unlock $lockurl
exit 0