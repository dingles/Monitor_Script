#!/bin/sh
#On AIX HP 
#sh topcpu.sh pos  1 1
#Before to apply,please midify these parameter.
#UNIX95= ps -e -o "pcpu,vsz,args,pid"|sort -rn|head -20|awk '{if($3== "oracleecology" && $1>5) print $2}' > $PROG_HOME/$TEMPFILE2
#head -20 $PROG_HOME/$TEMPFILE1|head -20|awk '{if($11== "oracleecology" && $3>0.2) print $2}' > $PROG_HOME/$TEMPFILE2
scriptsdir=$(cd `dirname $0`; pwd)/../../..
. ${scriptsdir}/conf/mn.cnf
PROG_HOME="${scriptsdir}/data"
ostype=`uname 2>/dev/null`
cpupct=0.1
sid=pos
upload=0
if [[ -n $1 && -n $2 ]];then
	case $ostype in
	  linux*|LINUX*|Linux*)
		. $HOME/.bash_profile
			;;
			AIX*|aix*)
		. $HOME/.profile
			;;
			HP*|hp*)
			. $HOME/.profile
			;;
	  *)
	   . $HOME/.profile
		;;
	esac
	cpupct=$2 
	sid=$1
	if [[ -n $3 && $3 -eq 1 ]];then
		upload=1
	fi
else
	echo "input parameter."
	exit 1
fi

matchsid="oracle${sid}"
ORACLE_SID=$sid;export ORACLE_SID
TEMPFILE1="$PROG_HOME/topsnap1.log"
TEMPFILE2="$PROG_HOME/topsnap2.log"
>$TEMPFILE1
>$TEMPFILE2
SPOOLFILE="$PROG_HOME/topspool.log"
SOURCE="$PROG_HOME/ps.src"
TMP="$PROG_HOME/ps.tmp"
REPORT="$PROG_HOME/ps.report"
Hour=`date +"%Y%m%d%H"`
time=`date +"%Y%m%d%H%M"`
TCREPORT="$PROG_HOME/cpu_report_${ORACLE_SID}_.$time.txt"

#get pid
case $ostype in
  linux*|LINUX*|Linux*)
		echo "This is Linux,No code to support."
        ;;
        AIX*|aix*)
			ps aux > $SOURCE
			head -1 $SOURCE > $REPORT
			grep -v "%CPU" $SOURCE | grep -v "^$"|grep "oracle" > $TMP
			sort +2nr +3nr $TMP >>$TEMPFILE1 
			head -20 $TEMPFILE1|head -20|grep $matchsid|grep "LOC"| awk -v var1=$cpupct '{if($3> var1) print $2}' > $TEMPFILE2
        ;;
        HP*|hp*)
			UNIX95= ps -e -o "pcpu,vsz,args,pid"|sort -rn|grep $matchsid|head -20|awk -v var1=$matchsid -v var2=$cpupct '{if($3== var1 && $1>var2) print $5}' > $TEMPFILE2
        ;;
  *)
    exit 0
esac

if [[ $(cat $TEMPFILE2|wc -l) -eq 0 ]] ;then
	exit 1
fi

cat $TEMPFILE2|awk '{print $1}'|while read temp_pid 
do
	sqlplus -s /nolog<<EOF
	connect / as sysdba;
	set head off 
	set linesize 141
	spool $PROG_HOME/result_topcpu.$temp_pid.log
		  set wrap off
		  set linesize 140
		  col machine for a20
		  col username for a20
		  col program for  a30
		  col process for a15
		  col sql_hash_value for 99999999999999 
		  col sql_text format a200
	select '--------------The OS Process Number is '||$temp_pid||' --------------' from dual;
	set head on
	select 'INFO os_pid:$temp_pid ',b.sql_hash_value sql_hash_value,b.machine machine,b.username username,
	b.program program,b.process process from v\$process a,v\$session b where a.addr=b.paddr and a.spid=$temp_pid;
	--insert into  perfstat.top_cpu_sql_1(instance_name,sid,serial#,sql_hash_value,sql_id,USERNAME,machine,PROGRAM,spid,c_date)
	--SELECT 'CRMDB01',b.sid sid,b.serial# serial#,b.sql_hash_value sql_hash_value,b.sql_id sql_id,b.username username, b.machine machine,
	--        b.program program, a.spid spid,sysdate
	--  FROM v\$process a, v\$session b
	-- WHERE a.addr = b.paddr AND a.spid = $temp_pid and b.machine in('bss2vac','crm1app','crm2app','prmdb','prmdb_per','test','waihu01','waihu02','waihu03','waihu04','web1','web10','web2','web3','web4','web5','web6','web7','web8','web9','zwdb01');
	--insert into  perfstat.top_cpu_sql select * from perfstat.top_cpu_sql_1;
	--delete  from  perfstat.top_cpu_sql_1;
	--commit;
	--exec perfstat.create_top_sql_new_one($temp_pid)

	set pagesize 0
	select a.sql_text from v\$sqltext a,v\$process b,v\$session c where a.hash_value=c.sql_hash_value and b.addr=c.paddr and b.spid=$temp_pid order by piece; 
		  set heading off
	select '--------------------------------------------------------------------------------' from dual
	union all
	select '| Operation | PHV/Object Name | Rows | Bytes| Cost |' as "Optimizer Plan:" from dual
	union all
	select '--------------------------------------------------------------------------------' from dual
	union all
	select *
	from (select
	rpad('|'||substr(lpad(' ',1*(a.depth-1))||a.operation||
	decode(a.options, null,'',' '||a.options), 1, 62), 63, ' ')||'|'||
	rpad(decode(a.id, 0, '----- '||to_char(a.hash_value)||' -----'
	, substr(decode(substr(a.object_name, 1, 7), 'SYS_LE_', null, a.object_name)
	||' ',1, 20)), 21, ' ')||'|'||
	lpad(decode(a.cardinality,null,' ',
	decode(sign(a.cardinality-10000), -1, a.cardinality||' ',
	decode(sign(a.cardinality-1000000), -1, trunc(a.cardinality/1000)||'K',
	decode(sign(a.cardinality-1000000000), -1, trunc(a.cardinality/1000000)||'M',
	trunc(a.cardinality/1000000000)||'G')))), 7, ' ') || '|' ||
	lpad(decode(a.bytes,null,' ',
	decode(sign(a.bytes-1024), -1, a.bytes||' ',
	decode(sign(a.bytes-1048576), -1, trunc(a.bytes/1024)||'K',
	decode(sign(a.bytes-1073741824), -1, trunc(a.bytes/1048576)||'M',
	trunc(a.bytes/1073741824)||'G')))), 6, ' ') || '|' ||
	lpad(decode(a.cost,null,' ',
	decode(sign(a.cost-10000000), -1, a.cost||' ',
	decode(sign(a.cost-1000000000), -1, trunc(a.cost/1000000)||'M',
	trunc(a.cost/1000000000)||'G'))), 8, ' ') || '|' as "Explain plan"
	from v\$sql_plan a,v\$session b,v\$process c
	where a.hash_value = b.sql_hash_value and b.paddr=c.addr and c.spid=$temp_pid)
	union all
	select '--------------------------------------------------------------------------------' from dual;

	spool off
	quit
EOF
done

cat $PROG_HOME/result_topcpu.*.log | grep INFO | grep -v "SQL_HASH_VALUE" > $TCREPORT
if [[ $upload -eq 1 ]];then
	if [[ $dataformat -eq 1 ]];then
		while read LINE
		do
			newline=$(echo $LINE|sed "s/'/ /g")
			#echo "insert into mn_node_log values('$rid.db.{$ORACLE_SID}','oradb_topcpu','WARNNING','$newline','$TCREPORT',sysdate)" 
			echo "insert into mn_node_log values('$rid.db.{$ORACLE_SID}','oradb_topcpu','WARNNING','$newline','file:$TCREPORT,cpupct=$cpupct',sysdate)">>$dataformaturl
			echo "--NEXT" >>$dataformaturl
		done < $TCREPORT
	fi
fi
cat $PROG_HOME/result_topcpu.*.log>$TCREPORT
rm -f $PROG_HOME/result_topcpu.*.log >/dev/null
find $PROG_HOME/ -mtime +7 -name "cpu_report*.txt" -exec rm -f {} \;
exit 0
