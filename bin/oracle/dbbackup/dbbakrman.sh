#!/bin/sh
#run on HP and IBM  and Linux
#Hown to:
#sh run.sh $scriptsdir 
#pars:
#$1 scriptsdir
#$2 oraclesid
##parameter about backup
#     schedule type is                BACKUP_TYPE is
#     ----------------                --------------
# Automatic Full                     INCREMENTAL LEVEL=0
# Automatic Differential Incremental INCREMENTAL LEVEL=1     
# Automatic Cumulative Incremental   INCREMENTAL LEVEL=1 CUMULATIVE
# replace "sys/manager", below, with the target connect string.

ftpupload()
{
##################################################
#in 
#   $1 host
#   $2 user
#   $3 passwd
#   $4 remote_dir
#   $5 local_dir
#   $6 files
#out
#   Y up to $3/$today
#   N 
##############################################
# if [[ -n $1 ]];then
#     echo "secussfully">/dev/null
# else 
#     echo "parameter is errr."
#     return 1;
# fi
host=$1
user=$2
passwd=$3
remote_dir=$4
local_dir=$5
file_name=$6
today=$(date "+%Y%m%d")
ftp_tmpfile=/tmp/ftp.$$.log 
###########start upload
ftp -in >$ftp_tmpfile << EOF
open $host
user $user $passwd
bin
cd $remote_dir
mkdir $today
cd $today
lcd $local_dir
prompt off
prompt off
mput $file_name
bye
EOF
}

######Init############################################
#pars
scriptsdir=$1
#include lib
ostype=`uname 2>/dev/null`
case $ostype in
    HP*|hp*)
    . $HOME/.profile
    ;;
    linux*|LINUX*|Linux*)
    . $HOME/.bash_profile
    ;;
    *)
    . $HOME/.profile
    ;;
esac
if [ $2 ];then
     export ORACLE_SID=$2
fi
#env

CTIME=`date +%Y%m%d%H%M%S`
CTIME2=`date +%Y%m%d`
ORACLE_USER=`id |cut -d"(" -f2 | cut -d ")" -f1`
BASE_DIR=${scriptsdir}/data/dbbackup

BK_DIR=${BASE_DIR}/${CTIME2}
RMAN_LOG_FILE=${BK_DIR}/`hostname`_backup_db.$CTIME.log
BACKUP_TAG=full_$CTIME
NB_ORA_FULL=1
NB_ORA_INCR=0
NB_ORA_CINC=0
BACKUP_DB=${BK_DIR}/$ORACLE_SID.db_____%T_%s_%t.bak
BACKUP_ARCH=${BK_DIR}/$ORACLE_SID.arch___%T_%s_%t.bak
BACKUP_CTL=${BK_DIR}/$ORACLE_SID.ctl____%T_%s_%t.bak
BACKUP_SPFILE=${BK_DIR}/$ORACLE_SID.spfile_%T_%s_%t.bak
TARGET_CONNECT_STR=/
RMAN=$ORACLE_HOME/bin/rman

#mkdir the backup dir
if [[ -d "$BK_DIR" ]];then
    echo "The backup file exist."
    exit 
else
    mkdir -p $BK_DIR
fi

#create cmdstr
echo >> $RMAN_LOG_FILE
#information in log
echo Script $0                           >> $RMAN_LOG_FILE
echo ==== started on `date` ====         >> $RMAN_LOG_FILE
echo                                     >> $RMAN_LOG_FILE
echo   "RMAN: $RMAN"                     >> $RMAN_LOG_FILE
echo   "ORACLE_SID: $ORACLE_SID"         >> $RMAN_LOG_FILE
echo   "ORACLE_USER: $ORACLE_USER"       >> $RMAN_LOG_FILE
echo   "ORACLE_HOME: $ORACLE_HOME"       >> $RMAN_LOG_FILE
echo                                     >> $RMAN_LOG_FILE
echo   "NB_ORA_FULL: $NB_ORA_FULL"       >> $RMAN_LOG_FILE
echo   "NB_ORA_INCR: $NB_ORA_INCR"       >> $RMAN_LOG_FILE
echo   "NB_ORA_CINC: $NB_ORA_CINC"       >> $RMAN_LOG_FILE
echo                                     >> $RMAN_LOG_FILE

#check
if [ "$NB_ORA_FULL" = "1" ]
then
        echo "Full backup requested"                    >> $RMAN_LOG_FILE
        BACKUP_TYPE="INCREMENTAL LEVEL=0"
elif [ "$NB_ORA_INCR" = "1" ]
then
        echo "Differential incremental backup requested" >> $RMAN_LOG_FILE
        BACKUP_TYPE="INCREMENTAL LEVEL=1" 
elif [ "$NB_ORA_CINC" = "1" ]
then
        echo "Cumulative incremental backup requested"   >> $RMAN_LOG_FILE
        BACKUP_TYPE="INCREMENTAL LEVEL=1 CUMULATIVE" 
elif [ "$BACKUP_TYPE" = "" ]
then
        echo "Default - Full backup requested"            >> $RMAN_LOG_FILE
        BACKUP_TYPE="INCREMENTAL LEVEL=0"
fi

#command strings
CMD_STR="
ORACLE_HOME=$ORACLE_HOME
export ORACLE_HOME
ORACLE_SID=$ORACLE_SID
export ORACLE_SID
$RMAN target $TARGET_CONNECT_STR   << EOF
RUN {
BACKUP
    $BACKUP_TYPE
    SKIP INACCESSIBLE
    TAG $BACKUP_TAG
    FILESPERSET 15
    # recommended format
    FORMAT 
    '$BACKUP_DB'
    DATABASE;
# the control file should be backed up as the last step of the RMAN section.  This step would not be
# necessary if we were using a recovery catalog.
sql 'alter system archive log current';
change archivelog all crosscheck;
BACKUP  
   FORMAT 
   '$BACKUP_ARCH'
ARCHIVELOG ALL delete input; 
BACKUP
    FORMAT
    '$BACKUP_SPFILE'
    SPFILE;
BACKUP
    FORMAT 
    '$BACKUP_CTL'
    CURRENT CONTROLFILE;
}
EOF
"
echo  RMAN Scripts >> $RMAN_LOG_FILE
echo  $CMD_STR     >>$RMAN_LOG_FILE
#start to backup
/bin/sh -c "$CMD_STR" >> $RMAN_LOG_FILE

ftpupload 192.168.188.101 oracle oracle /ftp/backup/webvip $BK_DIR *.bak

exit 0

