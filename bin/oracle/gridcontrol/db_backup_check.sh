#!/bin/sh
#######################################
#$1 db backup
#   full
#   incr_0
#   incr_1
#   archivelog
#   exp
#   ctas
#$2 sid
#
#On grid control user,user perl to exec it
#file:db_backup_check.perl
#[code]
#!/usr/bin/perl
#my $res = `/usr/bin/sh /mon/mon/db_backup_check.sh $ARGV[0]`; 
#print $res;
#exit 0;
#[/code]
#######################################
#case $ostype in
#  linux*|LINUX*|Linux*)
#    . $HOME/.bash_profile >/dev/null
# ;;
# AIX*|aix*)
#    . $HOME/.profile >/dev/null
# ;;
# HP*|hp*)
# . $HOME/.profile >/dev/null
# ;;
#  *)
#   . $HOME/.profile >/dev/null
#    ;;
#esac



#########if backup type is exp######
#flag is
#r_time=`date "+%Y%m%d"`
#echo "Exp is end,Time is:$r_time"
####################################
db_exp_log=/tmp/bi_backup.log
tab_cnt=2
#########end

export ORACLE_SID=BI
ORACLE_HOME=/oracle/ora10g/product/10.2.0
ostype=`uname 2>/dev/null`
hbak_day=100



spoolfile=/tmp/rman_chk.$$.tmp

case $1 in
  full )
    #this is backup controlfile
    chk_type="Full"
    run_cmd="rman"
;;
  incr_0 )
    chk_type="Incr-0"
    run_cmd="rman"
;;
  incr_1 )
    chk_type="Incr-1"
    run_cmd="rman"
;;
  archivelog )
    chk_type="Archivelog"
    run_cmd="rman"
;;
  exp)
    run_cmd="exp"
;; 
  ctas)
    run_cmd="ctas"
;;
  *)
    run_cmd="others"
;;
esac
########################main###########################
case $run_cmd in
  rman )
    $ORACLE_HOME/bin/sqlplus "/ as sysdba"   >/dev/null <<EOF
    set lines 100
    set pages 0
    col res for a15
    spool $spoolfile
    --sql
    --select decode(substr(num,1,1),'.','0'||num,num) from t1_number 
    select 'RESULT:' ||
           decode(substr(trunc(sysdate - decode(max(completion_time),
                                                null,
                                                sysdate - 100,
                                                max(completion_time)),
                               2),
                         1,
                         1),
                  '.',
                  '0' || trunc(sysdate - decode(max(completion_time),
                                                null,
                                                sysdate - 100,
                                                max(completion_time)),
                               2),
                  trunc(sysdate - decode(max(completion_time),
                                         null,
                                         sysdate - 100,
                                         max(completion_time)),
                        2)) res
      from (SELECT A.RECID "BACKUP SET",
                   A.SET_STAMP,
                   DECODE(B.INCREMENTAL_LEVEL,
                          '',
                          DECODE(BACKUP_TYPE, 'L', 'Archivelog', 'Full'),
                          1,
                          'Incr-1',
                          0,
                          'Incr-0',
                          B.INCREMENTAL_LEVEL) Type_LV,
                   B.CONTROLFILE_INCLUDED "inc_CTL",
                   DECODE(A.STATUS,
                          'A',
                          'AVAILABLE',
                          'D',
                          'DELETED',
                          'X',
                          'EXPIRED',
                          'ERROR') STATUS,
                   A.DEVICE_TYPE "Device Type",
                   A.START_TIME "Start Time",
                   A.COMPLETION_TIME Completion_Time,
                   --  A.ELAPSED_SECONDS "Elapsed Seconds",
                   --   a.BYTES / 1024 / 1024 / 1024 "size(G)",
                   A.TAG    "Tag",
                   A.HANDLE "Path"
              FROM GV\$BACKUP_PIECE A, GV\$BACKUP_SET B
             WHERE A.SET_STAMP = B.SET_STAMP
               AND A.DELETED = 'NO'
             ORDER BY A.COMPLETION_TIME DESC) res
     where status = 'AVAILABLE'
       and res.Type_lv = '$chk_type';
     spool off
     quit
EOF
    hbak_day=`cat  $spoolfile |grep "RESULT" |grep -v "select"|awk -F ':' '{print substr($2,0,4)}'`
    if [[ -f  "$spoolfile" ]];then
      rm -f $spoolfile >/dev/null
    fi 
;;
  exp )
    #check 
    if [[ -n "$db_exp_log" &&  -n "$tab_cnt" ]];then
      if [[ -f "$db_exp_log" ]];then
          exp_tab_cnt=`cat $db_exp_log|grep exported|wc -l`
          #echo $exp_tab_cnt
          if [[ "$exp_tab_cnt" -ge "$tab_cnt" ]];then
            #get the backup time
            #flag is Exp is end,Time is : 20130220
            rec_d=`cat $db_exp_log |grep "Exp is end"|awk -F ':' '{print $2}'`
            ###get the em_result
            $ORACLE_HOME/bin/sqlplus "/ as sysdba"   >/dev/null <<EOF
            set lines 100
            set pages 0
            col res for a15
            spool $spoolfile
            --sql
            select  'RESULT:'|| decode(substr(trunc(sysdate - to_date('$rec_d', 'yyyymmdd'), 2),
                     1,
                     1),
              '.',
              '0' || trunc(sysdate - to_date('$rec_d', 'yyyymmdd'), 2),
              trunc(sysdate - to_date('$rec_d', 'yyyymmdd'), 2))
            from dual;
            spool off
            quit
EOF
            hbak_day=`cat  $spoolfile |grep "RESULT" |grep -v "select"|awk -F ':' '{print substr($2,0,4)}'`
            if [[ -f "$spoolfile" ]];then
              rm -f $spoolfile >/dev/null
            fi 
          fi
      fi
    fi
;;
  ctas )
  hbak_day=100
;;
  others)
  hbak_day=100
;;
  *)
  hbak_day=100
;;
esac

#output
print "em_result=$hbak_day\n"
 
exit 0