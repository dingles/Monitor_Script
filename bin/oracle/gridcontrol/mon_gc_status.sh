#!/bin/sh
########################################
#used to monitor grid control 
#crontab
#		
#	1,6,11,16,21,26,31,36,41,46,51,56   * * * * /mon/mon/mon_gc.sh >/tmp/gc.tmp 2>$1 
#Run on Linux
#from ygrdba007@yeah.net
#user ygrdba007
#passwd 007ygrdba
########################################
do_warn()
{
#insert into mon$alert values(alert_seq.nextval,5,'TSM','Tsm backup check','backup check','message','N',sysdate,sysdate,null);
user=$1
pwd=$2
tns=$3
SQLPLUS=$4
id=$5
source=$6
source_type=$7
title=$8
msg=$9
$SQLPLUS  $user/$pwd@$tns <<EOF
insert into mon\$alert values(alert_seq.nextval,$id,'$source','$source_type','$title','$msg','N',sysdate,sysdate,null);
commit;
quit 0
EOF
}


user=$1
pwd=$2
tns=$3
ORACLE_HOME=$4
AGENT_HOME=$5
OMS_HOME=$6



error=0
err_gc=0

count=`ps -ef |grep $0 |grep -v grep |wc -l`
if [[ $count -gt 5 ]]; then
        echo "$0 already running"
        context="mon shell already running."
        do_warn $user $pwd $tns $ORACLE_HOME/bin/sqlplus 6 "grid control"  "grid control"   "grid control"  "$context"

        exit 1
fi

##check gc agent
r_msg=`$AGENT_HOME/bin/emctl status agent|tail -1` 

echo "Check start."
if [[ $r_msg = "Agent is Running and Ready" ]];then
	#ok
	echo "Agent is OK."
else
	err_gc="Agent is down."
	error=1
fi

#check gc oms and webtier
r_msg=`$OMS_HOME/bin/emctl status oms|grep Up|wc -l`
if [[ $r_msg -eq 2 ]];then
	#ok
	echo "Oms is OK"
else
	err_gc="Oms is down."
	error=1
fi 

#print error
if [[ $error -eq 1 ]];then
	#send email     "-a $warn_send_file"  
	context=`echo $err_gc`
	echo "Start to send email to administrator."
    do_warn $user $pwd $tns $ORACLE_HOME/bin/sqlplus  6 "grid control"  "grid control"   "grid control"  "$context"
	echo "Send mail end."
fi

echo "Check end."

#exit
exit 0


gc=
  (DESCRIPTION =
    (ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.188.72)(PORT = 1521))
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = gc)
    )
  )