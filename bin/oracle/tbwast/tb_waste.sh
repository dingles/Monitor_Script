#!/bin/sh
########################################
#tb_wast.sh
#
#support on oracle 10g.
#
########################################
oracle_user=mon
oracle_passwd=oracle
rebuild_sql_text_tmp=/tmp/rebuild_sql.$$.tmp
rebuild_sql_text_sql=/tmp/rebuild_sql.$$.sql
report_sql=/mon/mon/tb_wash.sql



ostype=`uname 2>/dev/null`
case $ostype in
  linux*|LINUX*|Linux*)
    . $HOME/.bash_profile
        ;;
        AIX*|aix*)
    . $HOME/.profile
        ;;
        HP*|hp*)
        . $HOME/.profile
        ;;
  *)
   . $HOME/.profile
    ;;
esac


sqlplus "/ as sysdba"<<EOF
exec p_tb_monitor ;
quit
EOF



sqlplus  "$oracle_user/$oracle_passwd"<<EOF
set feedback off
set pages 0
set head off
set echo off


----to construct sql for the tables which need to rebuild;
spool $rebuild_sql_text_tmp
@$report_sql
spool off
--report sql text
!echo "--SQL for move tables:">$rebuild_sql_text_sql;
!echo "--================================= ">>$rebuild_sql_text_sql;
!cat $rebuild_sql_text_tmp |grep move |grep -v SQL >>$rebuild_sql_text_sql;
!echo "--================================= ">>$rebuild_sql_text_sql;
!echo "--SQL for rebuild indexes:">>$rebuild_sql_text_sql;
!echo "--================================= ">>$rebuild_sql_text_sql;
!cat $rebuild_sql_text_tmp |grep rebuild |grep -v SQL >>$rebuild_sql_text_sql;
!echo "--================================= ">>$rebuild_sql_text_sql;
!echo "--SQL for gather statistics:">>$rebuild_sql_text_sql;
!echo "--================================= ">>$rebuild_sql_text_sql;
!cat $rebuild_sql_text_tmp |grep dbms_stats |grep -v SQL |grep -v select >>$rebuild_sql_text_sql;
--start to run
#i#@$rebuild_sql_text_sql
exit
EOF


#######exit
if [[ -f $rebuild_sql_text_tmp ]];then
        echo "$rebuild_sql_text_tmp is exist"
        rm -f $rebuild_sql_text_tmp>/dev/null
fi

if [[ -f $rebuild_sql_text_sql ]];then
        echo "$rebuild_sql_text_sql is exist"
        rm -f $rebuild_sql_text_sql>/dev/null
fi
exit 0

