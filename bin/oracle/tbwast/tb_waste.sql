----construct sql for move tables：
set feedback off;
set pages 100;
set heading on;
col table_name for a30;
set trimspool on;
set line 500
col script_for_gather_stat for a250
select 'alter table ' ||owner||'.'||table_name||' move;'
from 
 TB_MONITOR WHERE SGM_SPACE_MANAGEMENT IS NOT NULL AND
(
(SIZE_THRESHOLD IS NOT NULL AND SIZE_THRESHOLD<CURRENT_SIZE AND WASTE_THRESHOLD<CURRENT_WASTE)
or
(SIZE_THRESHOLD is null and WASTE_THRESHOLD<CURRENT_WASTE)
);
-----------------------------------------------------------------------
-----------------------------------------------------------------------
---after the sql to move tables be constructed,we need to construct sql for rebuild the indexes:
------表Move后，需要重建索引，以下是手工构建重建的语句：
------关于重建索引的并行度问题，如果在rebuild的时候指定并行度，那么，rebuild完成后最后将并行度改回来。
------alter index test.IDX_T99 noparallel;
-----否则，将倾向于走全表扫
-----------------------------------------------------------------------
-----------------------------------------------------------------------
select 'alter index '||owner||'.'||index_name||' rebuild;' from dba_indexes a where  (a.table_name,a.owner) in
(
SELECT table_name,owner FROM TB_MONITOR WHERE SGM_SPACE_MANAGEMENT IS NOT NULL AND
(
(SIZE_THRESHOLD IS NOT NULL AND SIZE_THRESHOLD<CURRENT_SIZE AND WASTE_THRESHOLD<CURRENT_WASTE)
or
(SIZE_THRESHOLD is null and WASTE_THRESHOLD<CURRENT_WASTE)
)
);
-----------------------------------------------------------------------
-----------------------------------------------------------------------
--------now, we construct sql for gather statistics for the tables:
--------the percent is up to the size of table such as:
--------表重建完毕后，需要收集一次统计信息
----不同的表采用不同的分析采样
--if size_m<=500 then  estimate_percent=>100
--if size_m between 500 and 1024  then estimate_percent=>50
--if size_m between 1024 and 5120  then estimate_percent=>10
--if size_m between 5120 and 10240  then estimate_percent=>3
--if size_m between 10240 and 20480   then estimate_percent=>1
--可以采用如下的语句一次性生成分析语句
-----------------------------------------------------------------------
-----------------------------------------------------------------------
select 'exec dbms_stats.gather_table_stats(OWNNAME=>' || '''' ||
       upper(owner) || ''',tabname=>' || '''' || upper(table_name) ||
       ''',cascade=>TRUE,degree=>4);' as script_for_gather_stat
  from (select owner,
               segment_name table_name,
               sum(bytes) / 1024 / 1024 size_m
          from dba_segments
         where (owner, segment_name) in
               (SELECT owner, table_name
                  FROM TB_MONITOR
                 WHERE SGM_SPACE_MANAGEMENT IS NOT NULL
                   AND ((SIZE_THRESHOLD IS NOT NULL AND
                       SIZE_THRESHOLD < CURRENT_SIZE AND
                       WASTE_THRESHOLD < CURRENT_WASTE) or
                       (SIZE_THRESHOLD is null and
                       WASTE_THRESHOLD < CURRENT_WASTE)))
         group by segment_name, owner
         order by size_m) a
 where a.size_m <= 500 ---------------------<500M
union all
select 'exec dbms_stats.gather_table_stats(OWNNAME=>' || '''' ||
       upper(owner) || ''',tabname=>' || '''' || upper(table_name) ||
       ''',cascade=>TRUE,estimate_percent=>50,degree=>4);' as script_for_gather_stat
  from (select owner,
               segment_name table_name,
               sum(bytes) / 1024 / 1024 size_m
          from dba_segments
         where (owner, segment_name) in
               (SELECT owner, table_name
                  FROM TB_MONITOR
                 WHERE SGM_SPACE_MANAGEMENT IS NOT NULL
                   AND ((SIZE_THRESHOLD IS NOT NULL AND
                       SIZE_THRESHOLD < CURRENT_SIZE AND
                       WASTE_THRESHOLD < CURRENT_WASTE) or
                       (SIZE_THRESHOLD is null and
                       WASTE_THRESHOLD < CURRENT_WASTE)))
         group by segment_name, owner
         order by size_m) a
 where a.size_m between 500 and 1024 --------500M-1G
union all
select 'exec dbms_stats.gather_table_stats(OWNNAME=>' || '''' ||
       upper(owner) || ''',tabname=>' || '''' || upper(table_name) ||
       ''',cascade=>TRUE,estimate_percent=>10,degree=>4);' as script_for_gather_stat
  from (select owner,
               segment_name table_name,
               sum(bytes) / 1024 / 1024 size_m
          from dba_segments
         where (owner, segment_name) in
               (SELECT owner, table_name
                  FROM TB_MONITOR
                 WHERE SGM_SPACE_MANAGEMENT IS NOT NULL
                   AND ((SIZE_THRESHOLD IS NOT NULL AND
                       SIZE_THRESHOLD < CURRENT_SIZE AND
                       WASTE_THRESHOLD < CURRENT_WASTE) or
                       (SIZE_THRESHOLD is null and
                       WASTE_THRESHOLD < CURRENT_WASTE)))
         group by segment_name, owner
         order by size_m) a
 where a.size_m between 1024 and 5120 -----------1G-5G
union all
select 'exec dbms_stats.gather_table_stats(OWNNAME=>' || '''' ||
       upper(owner) || ''',tabname=>' || '''' || upper(table_name) ||
       ''',cascade=>TRUE,estimate_percent=>3,degree=>4);' as script_for_gather_stat
  from (select owner,
               segment_name table_name,
               sum(bytes) / 1024 / 1024 size_m
          from dba_segments
         where (owner, segment_name) in
               (SELECT owner, table_name
                  FROM TB_MONITOR
                 WHERE SGM_SPACE_MANAGEMENT IS NOT NULL
                   AND ((SIZE_THRESHOLD IS NOT NULL AND
                       SIZE_THRESHOLD < CURRENT_SIZE AND
                       WASTE_THRESHOLD < CURRENT_WASTE) or
                       (SIZE_THRESHOLD is null and
                       WASTE_THRESHOLD < CURRENT_WASTE)))
         group by segment_name, owner
         order by size_m) a
 where a.size_m between 5120 and 10240 -----------5G-10G
union all
select 'exec dbms_stats.gather_table_stats(OWNNAME=>' || '''' ||
       upper(owner) || ''',tabname=>' || '''' || upper(table_name) ||
       ''',cascade=>TRUE,estimate_percent=>1,degree=>4);' as script_for_gather_stat
  from (select owner,
               segment_name table_name,
               sum(bytes) / 1024 / 1024 size_m
          from dba_segments
         where (owner, segment_name) in
               (SELECT owner, table_name
                  FROM TB_MONITOR
                 WHERE SGM_SPACE_MANAGEMENT IS NOT NULL
                   AND ((SIZE_THRESHOLD IS NOT NULL AND
                       SIZE_THRESHOLD < CURRENT_SIZE AND
                       WASTE_THRESHOLD < CURRENT_WASTE) or
                       (SIZE_THRESHOLD is null and
                       WASTE_THRESHOLD < CURRENT_WASTE)))
         group by segment_name, owner
         order by size_m) a
 where a.size_m > 10240;



