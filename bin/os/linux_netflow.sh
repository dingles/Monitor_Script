#!/bin/sh
#mn.cnf:dataformat,dataformaturl
#sar -n DEV 1 3
#Average:        IFACE   rxpck/s   txpck/s   rxbyt/s   txbyt/s   rxcmp/s   txcmp/s  rxmcst/s
#Average:           lo      2.68      2.68    453.85    453.85      0.00      0.00      0.00
#Average:         eth0      2.01      1.00    126.42    312.37      0.00      0.00      0.00
#just run on linux
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../..
#scriptsdir=$(cd `dirname $0`; pwd)
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
#env
lockurl=${scriptsdir}/locks/os.linux.net.lck
datafile=${scriptsdir}/data/os.linux.net.dat

if [ `islock $lockurl` -eq 1 ];then
	#reclog "OS.LINUX.NET" "ERROR:file is locked."
	exit 1
fi
lock $lockurl $$
##############main
sar -n DEV 1 3|grep -E "Average|平均时间"|grep -v IFACE|awk '{print $2,$5,$6}'|
while read f1 f2 f3 
do
	#formatsqlfile
	if [[ $dataformat -eq 1 ]];then
		echo "insert into mn_os_netflow values('$rid','$f1',$f2,$f3,'$(date "+%Y%m%d:%H%M%S")',sysdate)">>$dataformaturl
		echo "--NEXT">>$dataformaturl
	fi
done
######Exit############################################
unlock $lockurl
exit 0
 
