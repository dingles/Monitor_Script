#!/bin/sh
#mn.cnf:dataformat,dataformaturl
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
#. ${scriptsdir}/lib/sysf/reclog.fun
#env
lockurl=${scriptsdir}/locks/os.load.lck
datafile=${scriptsdir}/data/os.load.dat
if [ `islock $lockurl` -eq 1 ];then
	#reclog  "OS.FS" "ERROR:file is locked."
	exit 1
fi
lock $lockurl $$
######Main############################################
#formatsqlfile
if [[ $dataformat -eq 1 ]];then
	echo "insert into mn_os_load values('$rid','`who|wc -l`','`ps -ef|wc -l`','`ps -ef|grep NO|grep -v grep|wc -l`','`ps -ef|grep ora_|grep -v grep|wc -l`','`uptime|awk -F ':' '{print $NF}'|awk -F ',' '{print $1}'`','$(date "+%Y%m%d:%H%M%S")',sysdate)" >>$dataformaturl
	echo "--NEXT" >>$dataformaturl
fi
######Exit############################################
unlock $lockurl
exit 0
