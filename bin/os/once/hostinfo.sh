#!/bin/sh
#mn.cnf:dataformat,dataformaturl
#just run on linux
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../../..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
. ${scriptsdir}/lib/sysf/os_cmd.fun
. ${scriptsdir}/lib/sysf/reclog.fun
#env
lockurl=${scriptsdir}/locks/os.hostinfo.lck.once
datafile=${scriptsdir}/data/os.hostinfo.dat
HNDETJSON=""

#process lock
if [[ `islock $lockurl` -eq 1 ]];then
	reclog  "OS.HOSTINFO" "ERROR:file is locked."
	exit 1
fi


lock $lockurl $$
#if linux OSTYPE LINUX
reclog  "MN | call $0 | INFO,start to run."
if [[ $OS_TYPE == "LINUX" ]];then
	ip=$(ifconfig -a|grep inet|grep -v 127.0.0.1|grep -v inet6|awk '{print $2}'|tr -d "addr:"|awk BEGIN{RS=EOF}'{gsub(/\n/," ");print}')
	HNDETJSON=$(echo "{\"type\":\"Linux\",\"ip\":\"$ip\",\"mem\":\"$(free -m|head -2|tail -1|awk '{print $2}')m\",\"cpu\":\"$(grep "process" /proc/cpuinfo|wc -l)\"}")
elif [[ $OS_TYPE == "AIX" ]];then
	ip=$(ifconfig -a|grep inet|grep -v 127.0.0.1|grep -v inet6|awk '{print $2}'|awk BEGIN{RS=EOF}'{gsub(/\n/," ");print}'  )
	HNDETJSON=$(echo {\"type\":\"AIX\",\"ip\":\"$ip\",\"cpu\":\"$(prtconf|grep "Number Of Processors"|awk -F ':' '{print $2}')\", \"mem\":\" $(prtconf|grep "Memory Size"|awk -F ':' '{print $2}'|tail -1|awk '{print $1}')m\"} )
elif [[ $OS_TYPE == "HP" ]];then
	ip=$(netstat -in|grep -v 127.0.0.1|grep -v Name|awk '{print $4}'|awk BEGIN{RS=EOF}'{gsub(/\n/," ");print}')
	HNDETJSON=$(echo {\"type\":\"HP\",\"ip\":\"$ip\",\"mem\":\"$(machinfo|grep Memory|awk -F "MB" '{print $1}'|awk -F '=' '{print $2}')m\",\"cpu\":\"$(machinfo|grep CPUs|awk -F '=' '{print $2}')\"})
else
	reclog  "MN | call $0 | WARN:No Code."
	exit 1
fi

######Main############################################
#formatsqlfile
if [[ $dataformat -eq 1 ]];then
	#echo "insert into mn_node_info values('$rid','$rid','$rid','HOST','$HNDETJSON','valid','desc',sysdate,sysdate,sysdate)" 
	echo "insert into mn_node_info values('$rid','$rid','$rid','HOST','$HNDETJSON','valid','desc',sysdate,sysdate,sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
fi
######Exit############################################
exit 0
