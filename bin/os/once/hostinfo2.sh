#!/bin/sh
#mn.cnf:dataformat,dataformaturl
#just run on linux
######Init############################################
scriptsdir=$(cd `dirname $0`; pwd)/../../..
#include libs and pars
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/lock.fun
. ${scriptsdir}/lib/sysf/os_cmd.fun
. ${scriptsdir}/lib/sysf/reclog.fun
#env
lockurl=${scriptsdir}/locks/os.hostinfo2.lck.once
datafile=${scriptsdir}/data/os.hostinfo2.dat

#process lock
if [[ -f $lockurl ]];then
	reclog  "OS.HOSTINFO2" "ERROR:file is locked."
	exit 1
fi

lock $lockurl $$
#if linux OSTYPE LINUX
reclog  "MN | call $0 | INFO,start to run."
type=$OS_TYPE
ip=""
mem=""
cpu=""
rid="testdf"

if [[ $OS_TYPE == "LINUX" ]];then
	ip=$(ifconfig -a|grep inet|grep -v 127.0.0.1|grep -v inet6|awk '{print $2}'|tr -d "addr:"|awk BEGIN{RS=EOF}'{gsub(/\n/," ");print}')
	mem=$(free -m|head -2|tail -1|awk '{print $2}')
	cpu=$(grep "process" /proc/cpuinfo|wc -l)
elif [[ $OS_TYPE == "AIX" ]];then
	ip=$(ifconfig -a|grep inet|grep -v 127.0.0.1|grep -v inet6|awk '{print $2}'|awk BEGIN{RS=EOF}'{gsub(/\n/," ");print}'  )
	mem=$(prtconf|grep "Memory Size"|awk -F ':' '{print $2}'|tail -1|awk '{print $1}')
	cpu=$(prtconf|grep "Number Of Processors"|awk -F ':' '{print $2}')
elif [[ $OS_TYPE == "HP" ]];then
	ip=$(netstat -in|grep -v 127.0.0.1|grep -v Name|awk '{print $4}'|awk BEGIN{RS=EOF}'{gsub(/\n/," ");print}')
	mem=$(machinfo|grep Memory|awk -F "MB" '{print $1}'|awk -F '=' '{print $2}')
	cpu=$(machinfo|grep CPUs|awk -F '=' '{print $2}')
else
	reclog  "MN | call $0 | WARN:No Code."
	exit 1
fi

######Main############################################
#formatsqlfile
if [[ $dataformat -eq 1 ]];then
	#NAME
	#AGENT
	#TYPE
	#IP
	#MEM
	#CPU
	#NODETYPE
	#CREATETIME
	#MARK 
	#ABOUT INFO
	echo "insert into mn_node_conf values('$rid','NAME','$rid','','','','','NODE','INFO',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	echo "insert into mn_node_conf values('$rid','AGENT','$rid','','','','','NODE','INFO',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	echo "insert into mn_node_conf values('$rid','TYPE','$type','','','','','NODE','INFO',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	echo "insert into mn_node_conf values('$rid','IP','$ip','','','','','NODE','INFO',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	echo "insert into mn_node_conf values('$rid','MEM','$mem','','','','','NODE','INFO',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	echo "insert into mn_node_conf values('$rid','CPU','$cpu','','','','','NODE','INFO',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	echo "insert into mn_node_conf values('$rid','NODETYPE','HOST','','','','','NODE','INFO',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	echo "insert into mn_node_conf values('$rid','CREATETIME',to_char(sysdate,'yyyy-mm-dd hh:mi:ss'),'','','','','NODE','INFO',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	echo "insert into mn_node_conf values('$rid','MARK','','','','','','NODE','INFO',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	#ABOUT REG SERVHCE
	echo "insert into mn_node_conf values('$rid','SERVICE','','SERVICE REG','','','','NODE','REG',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	#ABOUT REG METRIC
	echo "insert into mn_node_conf values('$rid','CPU','VALID','METRIC REG','85','90','','NODE','REG',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	echo "insert into mn_node_conf values('$rid','MEM','VALID','METRIC REG','999','999','','NODE','REG',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	echo "insert into mn_node_conf values('$rid','MEMSWAP','VALID','METRIC REG','100','100','','NODE','REG',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	echo "insert into mn_node_conf values('$rid','FS','VALID','METRIC REG','95','98','','NODE','REG',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	echo "insert into mn_node_conf values('$rid','LOAD','VALID','METRIC REG','5','10','','NODE','REG',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
	echo "insert into mn_node_conf values('$rid','STORAGE','VALID','METRIC REG','5','10','','NODE','REG',sysdate)">>$dataformaturl
	echo "--NEXT" >>$dataformaturl
fi
#cat $dataformaturl
######Exit############################################
exit 0
