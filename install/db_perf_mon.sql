create or replace procedure db_perf_mon is
  v_count2 number := 0;
  v_date2  number := 0;
  -- v_count1 number := 0;
  -- v_date1  number := 0;
  v_check number := 0;
  --v_msg1   varchar2(4000);
  v_msg2 varchar2(4000);
  cursor c1 is
    select a.instance_name,
           a.log_date,
           a.performance,
           trunc(a.event_count / 60, 2) avg_event_per_sec,
           a.event_count,
           b.event
      from (select instance_name,
                   log_date,
                   trunc(sum(decode(event,
                                    'ON CPU',
                                    event_count,
                                    'db file sequential read',
                                    event_count,
                                    0)) / sum(event_count),
                         2) performance,
                   max(decode(event,
                              'ON CPU',
                              0,
                              'db file sequential read',
                              0,
                              event_count)) event_count
              from (select instance_name,
                           trunc(log_date, 'mi') log_date,
                           event,
                           sum(event_count) event_count
                      from mon$db_perf_event
                     where (instance_name, log_date) in
                           (select instance_name, max(log_date)
                              from mon$db_perf_event
                             group by instance_name)
                       and event <> 'PX Deq Credit: send blkd'
                     group by instance_name, trunc(log_date, 'mi'), event)
             group by instance_name, log_date) a,
           (select instance_name,
                   trunc(log_date, 'mi') log_date,
                   event,
                   sum(event_count) event_count
              from mon$db_perf_event
             where (instance_name, log_date) in
                   (select instance_name, max(log_date)
                      from mon$db_perf_event
                     group by instance_name)
               and event <> 'PX Deq Credit: send blkd'
             group by instance_name, trunc(log_date, 'mi'), event) b
     where a.log_date = b.log_date
       and a.event_count = b.event_count
       and a.instance_name = b.instance_name
       and a.instance_name not like 'bc%'
    --and a.performance < 0.5
    --and a.event_count / 60 > 20
     order by a.log_date;
begin
  --性能监控，5分钟级别
  for r1 in c1 loop
    insert into mon$db_perf_record
    values
      (r1.instance_name,
       r1.log_date,
       r1.performance * 100,
       trunc(r1.EVENT_COUNT / 60),
       to_number(to_char(sysdate, 'DD')));
    commit;
    if r1.performance >= 0.5 and r1.avg_event_per_sec < 35 then
      null;
    else
			--获得在上一次数据
      select trunc(sum(event_count) / 60, 2)
        into v_check
        from mon$db_perf_event
       where log_date >= trunc(r1.log_date - 1 / 24 / 60, 'mi')
         and log_date < r1.log_date
         and event = r1.event
         and instance_name = r1.instance_name;
    
		  --上次等待事件数量每秒大于15次，本次等待时间数量每秒大于30 
      if v_check > 15 or r1.avg_event_per_sec > 30 then
				--效率低于25%
        if r1.performance < 0.25 then
          v_msg2 := to_char(r1.log_date, 'yyyy/mm/dd hh24:mi:ss') || ':' ||
                    r1.instance_name || ',数据库性能恶劣，主要异常为:' ||
                    substr(r1.event, 1, 15) || ',请重点关注!!';
        else
          v_msg2 := to_char(r1.log_date, 'yyyy/mm/dd hh24:mi:ss') || ':' ||
                    r1.instance_name || ',数据库性能低下，主要异常为:' ||
                    substr(r1.event, 1, 15) || ',请重点关注!!';
        end if;
				/*
        select count(*), to_number(to_char(sysdate, 'hh24'))
          into v_count2, v_date2
          from mon$db_perf_warn
         where log_date > sysdate - 1 / 24
           and log_db = r1.instance_name
           and log_option = r1.event
           and sms = 'Y'; */
				
        --if v_count2 > 0 or (v_date2 > 18 and r1.performance > 0.2) or(v_date2 < 8 and r1.performance > 0.2) or (r1.event = 'read by other session') then
				if (r1.event = 'read by other session') then
          insert into mon$db_perf_warn
          values
            (r1.log_date, r1.instance_name, r1.event, v_msg2, 'N');
          commit;
        else
          insert into mon$db_perf_warn
          values
            (r1.log_date, r1.instance_name, r1.event, v_msg2, 'Y');
          commit;
        end if;
      end if;
    end if;
  end loop;
end;
/
