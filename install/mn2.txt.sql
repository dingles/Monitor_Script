--包含node公告部分,附属部分及配置放置mn_sys_value
drop table mn_sys_conf;
create table mn_sys_conf
(
node varchar2(2000) not null,
nodename varchar2(1000),
status varchar2(1000) not null, --valid/invalid
type varchar2(100) not null,
mark varchar2(4000),
modifytime date,
inserttime date
)tablespace mn;
create unique index mnsysconfind1 on mn_sys_conf(node) tablespace mn;

drop table mn_sys_value;
create table mn_sys_value
(
node varchar2(2000),
itemtype varchar2(1000),--SERVIC_REG,METRIC_REG,NODE_SERVICE,NODE_INFO,NODE_METRIC
item varchar2(1000),
itemvaluea varchar2(1000),
itemvalueb varchar2(1000),
itemvaluec varchar2(1000),
itemvalued varchar2(1000),
itemvaluee varchar2(1000),
itemvaluef varchar2(1000),
itemvalueg varchar2(1000),
itemvalueh varchar2(1000),
inserttime date
)tablespace mn;
create unique index mnsysvalueind1 on mn_sys_value(node,itemtype,item) tablespace mn;
insert into mn_sys_value(node,itemtype,item,itemvaluea,itemvalueb) values('MN','SERVIC_REG','1B15E2753B7C80CDAD4E8D2A3DCCC2BE','默认组','valid');
insert into mn_sys_value(node,itemtype,item,itemvaluea,itemvalueb,itemvaluec,itemvalued,itemvaluee) values('MN','METRIC_REG','02F623B0FAB036837EFBD751F0A5CF6A','MEM','valid','INTER_HOST','conf','warn_yes');
insert into mn_sys_value(node,itemtype,item,itemvaluea,itemvalueb,itemvaluec,itemvalued,itemvaluee) values('MN','METRIC_REG','DD482526EB2EC6A700C0613DF4B85EEB','CPU','valid','INTER_HOST','conf','warn_yes');
insert into mn_sys_value(node,itemtype,item,itemvaluea,itemvalueb,itemvaluec,itemvalued,itemvaluee) values('MN','METRIC_REG','D4F95723B564FE817E613AC99AB1AFFA','IO','valid','INTER_HOST','conf','warn_yes');
insert into mn_sys_value(node,itemtype,item,itemvaluea,itemvalueb,itemvaluec,itemvalued,itemvaluee) values('MN','METRIC_REG','77761DF324F0F1FE081B7EF4132AB383','LOAD','valid','INTER_HOST','conf','warn_yes');

--select md5(to_char(sysdate,'yyyy-mm-dd hh24:mi:ss')) from dual;
--node global local 
insert into mn_sys_value(node,itemtype,item) values('nodeid','NODE_SERVICE','1B15E2753B7C80CDAD4E8D2A3DCCC2BE');
insert into mn_sys_value(node,itemtype,item,itemvaluea) values('nodeid','NODE_INFO','mem','8000');
insert into mn_sys_value(node,itemtype,item,itemvaluea) values('nodeid','NODE_INFO','cpu','8');
insert into mn_sys_value(node,itemtype,item,itemvaluea) values('nodeid','NODE_INFO','ip','192.168.1.1');
insert into mn_sys_value(node,itemtype,item,itemvaluea,itemvalueb,itemvalued) values('nodeid','NODE_METRIC','02F623B0FAB036837EFBD751F0A5CF6A','global','valid','');
insert into mn_sys_value(node,itemtype,item,itemvaluea,itemvalueb,itemvalued) values('nodeid','NODE_METRIC','DD482526EB2EC6A700C0613DF4B85EEB','global','valid','');
insert into mn_sys_value(node,itemtype,item,itemvaluea,itemvalueb,itemvalued) values('nodeid','NODE_METRIC','D4F95723B564FE817E613AC99AB1AFFA','global','valid','');
insert into mn_sys_value(node,itemtype,item,itemvaluea,itemvalueb,itemvalued) values('nodeid','NODE_METRIC','77761DF324F0F1FE081B7EF4132AB383','global','valid','');
 
--service
select b.serviceid,b.service,c.node,c.status from mn_sys_conf c,mn_sys_value a,(select item serviceid,itemvaluea service from  mn_sys_value where itemtype='SERVIC_REG')b
where a.itemtype='NODE_SERVICE' and a.item=b.serviceid and c.node=a.node

insert into mn_sys_value(node,itemtype,item,itemvaluea,itemvalueb) values('MN','SERVIC_REG','0CC175B9C0F1B6A831C399E269772661','测试组','valid');

node,mark,name,servicename,serviceid

 
 
 