

-----------------
--服务及目标表
-----------------
drop table mn_sys_service;
create table mn_sys_service
(
servicedef_id number,
target_id number
); 
create unique index mn_sys_service_ind1 on mn_sys_service(servicedef_id,target_id);
--insert into mn_sys_target values('POSQAS.1','pos host',8,null,sysdate)

drop table mn_sys_target;
create table mn_sys_target
(
target varchar2(100),   --nodeid+subid
target_alias  varchar2(555),
target_id number primary key,   
targetdef_id number,
target_metric_list varchar2(100),
cdate date
);
--序列
drop sequence seq_target;
create sequence seq_target
increment by 1 
start with 1 
nomaxvalue 
nocycle 
cache 10;
commit;

--目标自动发现表
drop table mn_sys_target_discover;
create table mn_sys_target_discover
(
target varchar2(100) primary key,
target_alias varchar2(100) not null,
targetdef_id number,
servicedef_id number,
cdate date
);


-----------------
--模版定义
-----------------
--服务及目标表
drop table mn_sys_servicedef;
create table mn_sys_servicedef
(
servicedef varchar2(100) not null,
servicedef_id number primary key,
cdate date not null
);
drop sequence seq_servicedef;
--序列
create sequence seq_servicedef
increment by 1 
start with 10
nomaxvalue 
nocycle 
cache 10;
insert into mn_sys_servicedef values('主机组',1,sysdate);
insert into mn_sys_servicedef values('数据库组',2,sysdate);
insert into mn_sys_servicedef values('代理组',3,sysdate);
commit;

--目标模版定义
drop table mn_sys_targetdef;
create table mn_sys_targetdef
(
targetdef varchar2(100) not null,   --oracle,host,
targetdef_id number  primary key,
metricdef_id varchar2(100),
cdate date
);
drop sequence seq_targetdef;
create sequence seq_targetdef
increment by 1 
start with 100 
nomaxvalue 
nocycle 
cache 10;
commit;
insert into mn_sys_targetdef values('主机',1,'1-2-3-4-5-6',sysdate);
insert into mn_sys_targetdef values('数据库Oracle',2,'21-22-23-24',sysdate);
insert into mn_sys_targetdef values('Agent',3,'20',sysdate) ;
commit;

--度量模版定义
drop table mn_sys_metricdef;
create table mn_sys_metricdef
(
metricdef varchar2(100) not null,
metricdef_id number primary key,
map varchar2(500),
notes varchar2(500),
cdate date not null
);
--序列
drop sequence seq_metricdef;
create sequence seq_metricdef
increment by 1 
start with 1025
nomaxvalue 
nocycle 
cache 10;

--host,1-20
insert into mn_sys_metricdef values('fs',1,'map','nodes',sysdate);
insert into mn_sys_metricdef values('cpu',2,'map','nodes',sysdate);
insert into mn_sys_metricdef values('memory',3,'map','nodes',sysdate);
insert into mn_sys_metricdef values('stoate',4,'map','nodes',sysdate);
insert into mn_sys_metricdef values('load',5,'map','nodes',sysdate);
insert into mn_sys_metricdef values('process',6,'map','nodes',sysdate);
insert into mn_sys_metricdef values('agent',20,'map','nodes',sysdate);
--oracle db
insert into mn_sys_metricdef values('连接数',21,'map','nodes',sysdate);
insert into mn_sys_metricdef values('表空间',22,'map','nodes',sysdate);
insert into mn_sys_metricdef values('状态',23,'map','nodes',sysdate);
insert into mn_sys_metricdef values('归档日志',24,'map','nodes',sysdate);
commit;

--创建视图
--os
create or replace view mn_sys_os_cpu as
select node target ,os_user usr ,os_sys sys,os_idle idle ,os_wio wio , inserttime cdate from mn_os_cpumem;

create or replace view mn_sys_os_fs as
select node target, fsize fsize,avail avail,trunc(avail/fsize*100,2)||'%' used,mountpoint mountpoint,inserttime cdate from mn_os_fs;

create or replace view mn_sys_os_load as
select node target ,os_load load, inserttime cdate from mn_os_load;

--oradb tbs
create or replace view mn_oradb_tbs as
select node||'_'||oraname target ,tbsname tbsname,sum_space sum_space,free_space free_space,used_rate||'%' used_rate, inserttime cdate from mon_oradb_tbs;

create or replace view mn_oradb_session as
select node||'_'||oraname target ,sesscnt,activesesscnt,inserttime cdate from mon_oradb_session

create or replace view mn_oradb_size as
select node||'_'||oraname target ,allocate allocate,free free,inserttime cdate from mon_oradb_size
--ymax=-999,则通过sql确定最大值
{
"title":"ORADB TBS",
"type":"table",
"ymin":0,
"ymax":100,     
"x":"time",
"tab":"mn_oradb_tbs",
"lengend":"tbsname-sum_space-free_space-used_rate",
"interval":0.5
}

{
"title":"File SYSTEM",
"type":"table",
"ymin":0,
"ymax":1,
"x":"time",
"tab":"mn_os_fs",
"lengend":"fsize-fsize-avail-used-mountpoint",
"interval":0.5
}

{
"title":"OS LOAD",
"type":"line",
"ymin":0,
"ymax":-999,
"x":"time",
"tab":"mn_os_load",
"lengend":"load",
"interval":0.5
}

{
"title":"OS CPU",
"type":"line",
"ymin":0,
"ymax":100,     
"x":"time",
"tab":"mn_os_cpu",
"lengend":"usr-sys-idle-wio",
"interval":0.5
}
