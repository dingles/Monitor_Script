ostype=`uname 2>/dev/null`
case $ostype in
	linux*|LINUX*|Linux*)
	. ~/.bash_profile
	;;
	AIX*|aix*)
	. ~/.profile
	;;
	HP*|hp*)
	. ~/.profile
	;;
	*)
	. ~/.profile
	;;
esac