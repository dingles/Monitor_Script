changedate()
{
#Usage:changedate [-b|-a] [n(days)] [Now(yyyymmdd)]
#check
if [[ ! -n $1 || ! -n $2 || ! -n $3 ]];then
    echo "parameter error."
    return 1
fi

if [[ $1 != "-a" && $1 != "-b" ]];then
	echo "parameter chose error."
	return 1
fi
Len=`expr length "$3"`
if [[ $Len -ne 8 ]];then
	echo "parameter date error."
	return 1
fi
#environment
Pre=$2
year=`echo $3|awk '{print substr($1,1,4)}'`
month=`echo $3|awk '{print substr($1,5,2)}'`
day=`echo $3|awk '{print substr($1,7,2)}'`
DateP=$3

#Process after day
if [ "$1" = "-a" ];then
    day=`expr $day + $Pre`
else
    day=`expr $day - $Pre`
fi

#get the maxday of every month
mon_max_day()
{
day=0
if [[ $1 -gt 0 && $1 -lt 13 ]];then
case $1 in
            1|01|3|03|5|05|7|07|8|08|10|12) 
                day=31
                ;;
            4|04|6|06|9|09|11) 
                day=30
                ;;
            2|02)
                if [ `expr $year % 4` -eq 0 ]; then
                        if [ `expr $year % 400` -eq 0 ]; then
                                day=29
                        elif [ `expr $year % 100` -eq 0 ]; then
                                day=28
                        else
                                day=29
                        fi
                else
                        day=28
                fi
                ;;
esac
fi
printf $day
}
##########Main Start
Max=`mon_max_day $month`
#Process After 
while [ $day -le 0 ]
do
        month=`expr $month - 1`
        if [ $month -eq 0 ];then
            month=12
            year=`expr $year - 1`
        fi
        Max=`mon_max_day $month`
        day=`expr $day + $Max`
done
#Process Before
while [ $day -gt $Max ]
do
        day=`expr $day - $Max`
        month=`expr $month + 1`
        if [ $month -eq 13 ];then
            month=1
            year=`expr $year + 1`
        fi
        Max=`mon_max_day $month`
done
DateA=`printf "%02d%02d%02d" $year $month $day`
echo "$DateA"
}
