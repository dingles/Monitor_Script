#!/bin/bash
Mnexeenvcheck()
{
. ${scriptsdir}/lib/sysf/os_cmd.fun
cmdschk=''

if [[ $OS_TYPE = "LINUX" ]];then
	cmdschk='df
	vmstat
	ifconfig
	sed
	'
elif [[ $OS_TYPE = "AIX" ]];then
	if [[ $(oslevel |awk -F '.' '{print $1}') -eq 6 ]];then
		echo "OS Check OK!"
	else	
		echo "OS CHeck failed."
		exit 1
	fi
	cmdschk='prtconf
	df
	vmstat
	ifconfig
	sed
	'
elif [[ $OS_TYPE = "HP" ]];then
	if [[ $(uname -r|grep 'B.11'|wc -l) -eq 1 ]];then
		echo "OS Check OK!"
	else	
		echo "OS CHeck failed."
		exit 1
	fi
	cmdschk='bdf
	vmstat
	ifconfig
	sed
	machinfo
	'
else
	echo "OS CHeck failed."
	exit 1
fi

for cmdstr in $cmdschk
do
	(which $cmdstr)>/dev/null 2>&1
	if [[ $? -gt 0 ]];then
		echo "CMD Check failed,$cmdstr is not exists."
		exit 1
	fi
done
echo "CMD Check OK!"
}
Mnstart()
{
sh ${scriptsdir}/bin/Mn.sh start
}

Mnstop()
{
sh ${scriptsdir}/bin/Mn.sh stop
}

Mnstatus()
{
sh ${scriptsdir}/bin/Mn.sh status
}

Mnrestart()
{
sh ${scriptsdir}/bin/Mn.sh restart
}

Mnmkcnf()
{
sh ${scriptsdir}/bin/Mn.sh mkcnf
}

Mn100log()
{
tail -100 ${scriptsdir}/alertmn.log
}

##########main###########################
scriptsdir=$(cd `dirname $0`; pwd)
. ${scriptsdir}/conf/mn.cnf
scriptsdir=$(cd `dirname $0`; pwd)
. ${scriptsdir}/lib/sysf/mn.fun
mnbanner

if [[ $1 = "restart" ]];then
	echo "Mn restart."
	Mnrestart
	exit 1
fi
echo "Now mn version:mn.v01.02.20150327"
PS3="Please input your choice:"
A="Mn start:Mn stop:Mn status:Mn restart:Mn mkcnf:Mn alertlog:exit" && IFS=:
select i in ${A};do
    case $i in
          "Mn start")
          echo "Mn start."
          Mnstart
          ;;
          "Mn stop")
          echo "Mn stop."
		  Mnstop
           ;;
          "Mn status")
          echo "Mn status."
		  Mnstatus
           ;;
          "Mn restart")
		  echo "Mn restart."
		  Mnrestart
           ;;
		  "Mn mkcnf")
		  echo "Mn mkcnf."
		  Mnmkcnf
		  ;;
		  "Mn alertlog")
		  echo "Mn alertlog."
		  Mn100log
		  ;;
           exit)
           exit 0 ;;
          *)
          echo "Not the right choice!";
		  ;;
    esac
done
exit 0
