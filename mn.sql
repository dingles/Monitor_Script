DROP TABLE MN_SYS_TAB;
CREATE TABLE MN_SYS_TAB(
ID NUMBER,
NAME VARCHAR2(100),
STATUS VARCHAR2(10), --VALID|INVALID
TYPE VARCHAR2(30), --SYS|NODEDATA
UDATE DATE
)TABLESPACE MN;
INSERT INTO MN_SYS_TAB SELECT ( SELECT DECODE(MAX(ID),NULL,1,MAX(ID)+1) FROM MN_SYS_TAB) ,'MN_NODE','VALID','SYS',SYSDATE FROM DUAL;
INSERT INTO MN_SYS_TAB SELECT ( SELECT DECODE(MAX(ID),NULL,1,MAX(ID)+1) FROM MN_SYS_TAB) ,'MN_SERVICE','VALID','SYS',SYSDATE FROM DUAL;
INSERT INTO MN_SYS_TAB SELECT ( SELECT DECODE(MAX(ID),NULL,1,MAX(ID)+1) FROM MN_SYS_TAB) ,'MN_METRIC_DEFINE','VALID','SYS',SYSDATE FROM DUAL;
INSERT INTO MN_SYS_TAB SELECT ( SELECT DECODE(MAX(ID),NULL,1,MAX(ID)+1) FROM MN_SYS_TAB) ,'MN_NODE_METRIC','VALID','SYS',SYSDATE FROM DUAL;
INSERT INTO MN_SYS_TAB SELECT ( SELECT DECODE(MAX(ID),NULL,1,MAX(ID)+1) FROM MN_SYS_TAB) ,'MN_USER_ALERTRSS','VALID','SYS',SYSDATE FROM DUAL;
INSERT INTO MN_SYS_TAB SELECT ( SELECT DECODE(MAX(ID),NULL,1,MAX(ID)+1) FROM MN_SYS_TAB) ,'MN_SYS_WARNMSG','VALID','SYS',SYSDATE FROM DUAL;
INSERT INTO MN_SYS_TAB SELECT ( SELECT DECODE(MAX(ID),NULL,1,MAX(ID)+1) FROM MN_SYS_TAB) ,'MN_SYS_DASHBOARD_REC','VALID','SYS',SYSDATE FROM DUAL;

DROP TABLE MN_NODE;
CREATE TABLE MN_NODE (
NODE VARCHAR2(500) NOT NULL ENABLE, 
NAME VARCHAR2(500), 
TYPEID NUMBER NOT NULL ENABLE, 
TYPE VARCHAR2(30) NOT NULL ENABLE, 
SUBTYPEID NUMBER NOT NULL ENABLE, 
SUBTYPE VARCHAR2(30) NOT NULL ENABLE, 
STATUS VARCHAR2(10) NOT NULL ENABLE, 
APPLYTEMPLATE NUMBER, --
MARK VARCHAR2(2000), 
CDATE DATE NOT NULL ENABLE, 
UDATE DATE
)TABLESPACE MN ;
CREATE UNIQUE INDEX MNNODEIND1 ON MN_NODE (NODE) TABLESPACE MN ;
CREATE INDEX MNNODEIND2 ON MN_NODE (NODE,TYPE,APPLYTEMPLATE,STATUS) TABLESPACE MN;
ALTER TABLE MN_NODE STORAGE(BUFFER_POOL KEEP);

DROP TABLE MN_NODE_DETAIL;
CREATE TABLE MN_NODE_DETAIL(    
NODE VARCHAR2(500) NOT NULL ENABLE, 
ITEM VARCHAR2(500) NOT NULL ENABLE, 
VALUE VARCHAR2(500)
) TABLESPACE MN;
ALTER TABLE MN_NODE_DETAIL STORAGE(BUFFER_POOL KEEP);
CREATE UNIQUE INDEX MNNODEDETIND2 ON MN_NODE_DETAIL (NODE,ITEM) TABLESPACE MN ;
 

DROP TABLE MN_SERVICE ;
CREATE TABLE MN_SERVICE (    
ID NUMBER NOT NULL ENABLE, 
NODE VARCHAR2(500) NOT NULL ENABLE, 
STATUS VARCHAR2(30),                --invalid|valid
TYPE VARCHAR2(100) NOT NULL ENABLE, --|SYS|INFO   
NAME VARCHAR2(100),
MARK VARCHAR2(500)
)TABLESPACE MN ;
CREATE UNIQUE INDEX MNSERVICEIND1 ON MN_SERVICE (ID, NODE, TYPE) TABLESPACE MN ;
ALTER TABLE MN_SERVICE STORAGE(BUFFER_POOL KEEP);
 
DROP TABLE MN_NODE_METRIC;
CREATE TABLE MN_NODE_METRIC(
NODE VARCHAR2(500) NOT NULL ENABLE,  
METRICID NUMBER NOT NULL ENABLE,
STATUS VARCHAR2(10) NOT NULL ENABLE, --启用停用度量valid|invalid
WARNCHECK VARCHAR2(10),              --valid
A VARCHAR2(100),
B VARCHAR2(100),
C VARCHAR2(100),
D VARCHAR2(100),
E VARCHAR2(100),
F VARCHAR2(100),
G VARCHAR2(100),
J VARCHAR2(100),
K VARCHAR2(100),
L VARCHAR2(100)
)TABLESPACE MN ;
CREATE INDEX MNNODEMETRIIND1 ON MN_NODE_METRIC (NODE, METRICID, STATUS,WARNCHECK) TABLESPACE MN ;
CREATE INDEX MNNODEMETRICIND2 ON MN_NODE_METRIC(METRICID,STATUS)TABLESPACE MN;
ALTER TABLE MN_NODE_METRIC STORAGE(BUFFER_POOL KEEP);

--------------------------定义模版--------------------------
--度量定义,肯定有模版
--[1-500 主机]
--[501-1000 oracle]
DROP TABLE MN_METRIC_DEFINE;
CREATE TABLE MN_METRIC_DEFINE (
ID NUMBER NOT NULL ENABLE, 
NAME VARCHAR2(100) NOT NULL ENABLE, 
INITCONF VARCHAR2(3000) NOT NULL ENABLE, 
SHOWCONF VARCHAR2(3000) NOT NULL ENABLE, 
WARNCONF VARCHAR2(3000) NOT NULL ENABLE, 
DATASOURCE VARCHAR2(100),   --snmp(网管协议)|agent(代理进程)|snmped(网管协议(已获取))
ADVISETYPEID NUMBER  NOT NULL ENABLE, 
ADVISETYPE VARCHAR2(100) NOT NULL ENABLE, --建议类型
MARK VARCHAR2(500)
)TABLESPACE MN ;
CREATE UNIQUE INDEX MNMETRICDEFINEIND1 ON MN_METRIC_DEFINE (ID) TABLESPACE MN;
ALTER TABLE MN_METRIC_DEFINE STORAGE(BUFFER_POOL KEEP);

DROP TABLE MN_METRIC_GROUP;
CREATE TABLE MN_METRIC_GROUP (
ID NUMBER,
NAME VARCHAR2(100), 
METRICID NUMBER
)TABLESPACE MN ;
CREATE UNIQUE INDEX MNMETRICGROUPIND1 ON MN_METRIC_GROUP (ID, METRICID) 
TABLESPACE MN;
ALTER TABLE MN_METRIC_GROUP STORAGE(BUFFER_POOL KEEP);

DROP TABLE MN_USER_ALERTRSS;
CREATE TABLE MN_USER_ALERTRSS(
USERID NUMBER,
NODE VARCHAR2(500),
STATUS VARCHAR2(10),  --valid|invalid
SERVICEID NUMBER    -- -999 
)TABLESPACE MN;
CREATE UNIQUE INDEX MNUSERALERTRSSND2 ON MN_USER_ALERTRSS (USERID, SERVICEID,NODE) 
TABLESPACE MN;
ALTER TABLE MN_USER_ALERTRSS STORAGE(BUFFER_POOL KEEP);

DROP TABLE MN_USER;
CREATE TABLE MN_USER(    
ID NUMBER, 
STATUS VARCHAR2(10), 
NAME VARCHAR2(100), 
MAIL VARCHAR2(100), 
SMS VARCHAR2(100), 
WARNALL VARCHAR2(10),
MAILLEVEL NUMBER NOT NULL, 
MAILOPTION VARCHAR2(10), 
SMSLEVEL NUMBER NOT NULL, 
SMSOPTION VARCHAR2(10), 
MARK VARCHAR2(500)
)TABLESPACE MN ;
CREATE INDEX MNUSERND1 ON MN_USER (ID,STATUS) TABLESPACE MN;
ALTER TABLE MN_USER STORAGE(BUFFER_POOL KEEP);

DROP TABLE MN_SYS_MAIL;
CREATE TABLE MN_SYS_MAIL(
STMP  VARCHAR2(100),
PORT NUMBER,
USERNAME VARCHAR2(100),
PWD VARCHAR2(100)
)TABLESPACE MN;
ALTER TABLE MN_SYS_MAIL STORAGE(BUFFER_POOL KEEP);

DROP TABLE MN_SYS_MAPPING;
CREATE TABLE MN_SYS_MAPPING
(ITEMA VARCHAR2(100),
ITEMB NUMBER,
VALUE VARCHAR2(500),
TYPE VARCHAR2(100)
) TABLESPACE MN ;
ALTER TABLE MN_SYS_MAPPING STORAGE(BUFFER_POOL KEEP);
INSERT INTO MN_SYS_MAPPING VALUES(0,NULL,'信息','SYSWARNLEVEL');
INSERT INTO MN_SYS_MAPPING VALUES(1,NULL,'警告','SYSWARNLEVEL');
INSERT INTO MN_SYS_MAPPING VALUES(2,NULL,'严重','SYSWARNLEVEL');
INSERT INTO MN_SYS_MAPPING VALUES(3,NULL,'紧急','SYSWARNLEVEL');

DROP TABLE MN_SYS_WARNMSG;
CREATE TABLE MN_SYS_WARNMSG(
SEQ VARCHAR2(100),
ATTACHINFO  VARCHAR2(100), 
ATTACHSTATUS VARCHAR2(10),
NODE VARCHAR2(500),
METRICID NUMBER,
WARNVALUE VARCHAR2(100),
WARNLEVEL NUMBER,
ITIME DATE,
CHECKTIME DATE
)TABLESPACE MN;
CREATE INDEX MNSYSWARNMSGIND1 ON MN_SYS_WARNMSG (SEQ) TABLESPACE MN;

DROP TABLE MN_SYS_DASHBOARD_REC;
CREATE TABLE MN_SYS_DASHBOARD_REC(
SEQ VARCHAR2(100),
WARNMSGSEQ VARCHAR2(100),
ATTACHINFO  VARCHAR2(100), 
NODE VARCHAR2(500),
NODENAME VARCHAR2(100),
NODETYPEID NUMBER,
NODETYPE VARCHAR2(100),
NODEMARK VARCHAR2(100),
METRICID NUMBER,
METRICNAME VARCHAR2(100),
CONTENT VARCHAR2(2000),
WARNVALUE VARCHAR2(100),
WARNTHRESHOLDVALUE VARCHAR2(100),
WARNLEVEL NUMBER,
WARNLEVELNAME VARCHAR2(100),
ITIME DATE,
CHECKTIME DATE,
IFSHOW VARCHAR2(30),--true|false
IFWARN VARCHAR2(30)--true|false
)TABLESPACE MN ;
ALTER TABLE MN_SYS_DASHBOARD_REC STORAGE(BUFFER_POOL KEEP);
CREATE UNIQUE INDEX MNDASHBOARDRECIND11 ON MN_SYS_DASHBOARD_REC(SEQ) TABLESPACE MN;
CREATE UNIQUE INDEX MNDASHBOARDRECIND22 ON MN_SYS_DASHBOARD_REC(SEQ,NODE,METRICID,ITIME) TABLESPACE MN;
CREATE INDEX MNDASHBOARDRECIND33 ON MN_SYS_DASHBOARD_REC(IFSHOW) TABLESPACE MN;
CREATE INDEX MNDASHBOARDRECIND44 ON MN_SYS_DASHBOARD_REC(NODE,METRICID,ITIME) TABLESPACE MN;

DROP TABLE MN_SYS_JOB;
CREATE TABLE MN_SYS_JOB
(
SEQ VARCHAR2(100),   --
NODE VARCHAR2(500),
JOBNAME VARCHAR2(100),   --JOBNAME
JOBGROUP VARCHAR2(100),  --
JOBCLASSNAME VARCHAR2(100),   --CLASSNAME
JOBCRON VARCHAR2(500),
JOBTYPE VARCHAR2(100),   --SYS|NODE|LOCAL|
JOBENABLE VARCHAR2(10),  --TRUE(计划中)|FALSE(计划外)
MARK VARCHAR2(1000),
RECDATE DATE
)TABLESPACE MN ;
ALTER TABLE MN_SYS_JOB STORAGE(BUFFER_POOL KEEP);
CREATE UNIQUE INDEX MNSYSJOBIND1 ON MN_SYS_JOB(SEQ) TABLESPACE MN;
CREATE UNIQUE INDEX MNSYSJOBIND2 ON MN_SYS_JOB(NODE,JOBNAME,JOBGROUP) TABLESPACE MN;
CREATE INDEX MNSYSJOBIND3 ON MN_SYS_JOB(NODE,JOBTYPE,JOBENABLE) TABLESPACE MN;
 
DROP TABLE MN_SYS_JOB_DTL;
CREATE TABLE MN_SYS_JOB_DTL(
SEQ VARCHAR2(100),
JOBSEQ VARCHAR2(500),
JOBCONTENT VARCHAR2(1000), --node + what
OPERTYPE VARCHAR2(30),  --execute|create|delete|modify|create
STATUS VARCHAR2(30),
RECLOG VARCHAR2(500),
STIME DATE,
ETIME DATE
)TABLESPACE MN;
ALTER TABLE MN_SYS_JOB_DTL STORAGE(BUFFER_POOL KEEP);
CREATE INDEX MNJOBDTLIND1 ON MN_SYS_JOB_DTL(SEQ) TABLESPACE MN;
CREATE INDEX MNJOBDTLIND2 ON MN_SYS_JOB_DTL(JOBSEQ) TABLESPACE MN;

DROP TABLE MN_NODE_HEARTBEAT;
CREATE TABLE MN_NODE_HEARTBEAT  
(NODE  VARCHAR2(500), 
 STATUS  VARCHAR2(10), 
 UPDATETIME  DATE
)TABLESPACE  MN  ;
ALTER TABLE MN_NODE_HEARTBEAT STORAGE(BUFFER_POOL KEEP); 
CREATE INDEX  MNHB_IND1  ON  MN_NODE_HEARTBEAT  ( UPDATETIME ) ;
create or replace view mn_node_hb_view as
select node,max(updatetime) lastdate from mn_node_heartbeat group by node;


--------------------------------------备份相关-----------------------------------------
drop table mn_sys_bknode;
create table mn_sys_bknode(
id varchar2(500),
name varchar2(500),  --oracle pos备份
status varchar2(20), --valid
call varchar2(100),
target varchar2(100),   --TSM SERVER
type varchar2(100),     --Oracle数据库|文件|
bkmethod varchar2(100),  --rman|exp|ftp|zip
policy varchar2(500),    --
mark varchar2(1000)
)tablespace mn;
create unique index mnsysbknodeind2 on mn_sys_bknode(id);
create index mnsysbknodeind1 on mn_sys_bknode(status,type);
ALTER TABLE mn_sys_bknode STORAGE(BUFFER_POOL KEEP);
insert into mn_sys_bknode values('0CC175B9C0F1B6A831C399E269772661','POS数据库','valid','tsm','TSM 带库','Oracle数据库','rman','policy','NaN');
insert into mn_sys_bknode values('92EB5FFEE6AE2FEC3AD71C777531578F','OA数据库','valid','tsm','TSM 带库','Oracle数据库','rman','policy','NaN');
insert into mn_sys_bknode values('21AD0BD836B90D08F4CF640B4C298E7C','DRP数据库','valid','tsm','TSM 带库','Oracle数据库','rman','policy','NaN');
insert into mn_sys_bknode values('08F8E0260C64418510CEFB2B06EEE5CD','BI数据库','valid','tsm','TSM 带库','Oracle数据库','rman','policy','NaN');
insert into mn_sys_bknode values('65BA841E01D6DB7733E90A5B7F9E6F80','PortalSOA数据库','valid','tsm','TSM 带库','Oracle数据库','rman','policy','NaN');
insert into mn_sys_bknode values('343D9040A671C45832EE5381860E2996','BICM数据库','valid','tsm','TSM 带库','Oracle数据库','rman','policy','NaN');
insert into mn_sys_bknode values('99754106633F94D350DB34D548D6091A','SCM数据库','valid','ftp','192.168.196.85','Oracle数据库','exp','policy','NaN');
insert into mn_sys_bknode values('EF1CB6E72D149B184CC241037203F60B','VIP数据库','valid','ftp','192.168.196.85','Oracle数据库','exp','policy','NaN');
insert into mn_sys_bknode values('DEC46145E2C5FA1E6A9EDF0820566796','呼叫中心数据库','valid','ftp','192.168.196.85','Mysql数据库','sql','policy','NaN');

drop table mn_sys_bknode_dtl;
create table mn_sys_bknode_dtl(
id varchar2(500),
name varchar2(100),
value varchar2(100)
)tablespace mn;
create index mnsysbknodedtlind1 on mn_sys_bknode_dtl(id,name);
create unique index mnsysbknodedtlind2 on mn_sys_bknode_dtl(value);
--当时tsmcallname时,保证唯一性
insert into mn_sys_bknode_dtl values('0CC175B9C0F1B6A831C399E269772661','tsmcallname','POSSRORAARCH');
insert into mn_sys_bknode_dtl values('0CC175B9C0F1B6A831C399E269772661','tsmcallname','POSSRORAFULL'); 
insert into mn_sys_bknode_dtl values('92EB5FFEE6AE2FEC3AD71C777531578F','tsmcallname','OADB-HPFILEIN');
insert into mn_sys_bknode_dtl values('92EB5FFEE6AE2FEC3AD71C777531578F','tsmcallname','OADB-HPFILEFULL');
insert into mn_sys_bknode_dtl values('21AD0BD836B90D08F4CF640B4C298E7C','tsmcallname','DRPPRDORADBARCH');
insert into mn_sys_bknode_dtl values('21AD0BD836B90D08F4CF640B4C298E7C','tsmcallname','DRPPRDORADBFULL');  
insert into mn_sys_bknode_dtl values('08F8E0260C64418510CEFB2B06EEE5CD','tsmcallname','ONLINE_BIDB_INCR');
insert into mn_sys_bknode_dtl values('08F8E0260C64418510CEFB2B06EEE5CD','tsmcallname','ONLINE_BIDB_FULL');
insert into mn_sys_bknode_dtl values('343D9040A671C45832EE5381860E2996','tsmcallname','ONLINE_BICM_ORACLE');
insert into mn_sys_bknode_dtl values('65BA841E01D6DB7733E90A5B7F9E6F80','tsmcallname','PORTAL_A1FULL');
insert into mn_sys_bknode_dtl values('99754106633F94D350DB34D548D6091A','ftpcallname','xxbfull');
insert into mn_sys_bknode_dtl values('EF1CB6E72D149B184CC241037203F60B','ftpcallname','vipdbfull');
insert into mn_sys_bknode_dtl values('DEC46145E2C5FA1E6A9EDF0820566796','ftpcallname','callcenterfull');

drop table mn_sys_bkrec;
create table mn_sys_bkrec(
id varchar2(500),
seq varchar2(500),
subname varchar2(500),
status varchar2(100),
pretime date not null,  --必须有,且正确
stime date,
etime date,
bksize number,
msg varchar2(1000),
mark varchar2(1000),
recmd5 varchar2(100),
inserttime date
)tablespace mn;
create index mnsysbkrecind1 on mn_sys_bkrec(inserttime);
create unique index mnsysbkrecind2 on mn_sys_bkrec(recmd5);
ALTER TABLE mn_sys_bkrec STORAGE(BUFFER_POOL KEEP);
 
--

drop table mn_sys_bkrec_dtl_source;
create table mn_sys_bkrec_dtl_source(
seq number,
source varchar2(1000), --192.168.196.85
type varchar2(100),    --tsm|ftp
A varchar2(1000),
B varchar2(1000),
C varchar2(1000),
D varchar2(1000),
E varchar2(1000),
F varchar2(1000),
G varchar2(1000),
H varchar2(1000),
I varchar2(1000),
J varchar2(1000),
K varchar2(1000),
L varchar2(1000),
mark varchar2(1000),
inserttime date
)tablespace mn;
drop SEQUENCE seqbkrec_dtl_source;
CREATE SEQUENCE seqbkrec_dtl_source
INCREMENT BY 1 -- 每次加几个
START WITH 1 -- 从1开始计数
NOMAXvalue -- 不设置最大值
NOCYCLE -- 一直累加，不循环
CACHE 10; --设置缓存cache个序列，如果系统down掉了或者其它情况将会导致序列不连续，也可以设置为---------NOCACHE 

 
drop table mn_dt_os_cpu;
create table mn_dt_os_cpu(
node varchar2(500),
usr number,
sys number,
idle number,
wio number,
inserttime date
)tablespace mn;


drop table mn_dt_os_load;
create table mn_dt_os_load(
node varchar2(500),
load number,
users number,
processes number,
inserttime date
)tablespace mn;


create table mn_os_netconn(
node varchar2(500),

inserttime(500),
)tablespace mn;
CLOSED  //无连接是活动的或正在进行

LISTEN  //服务器在等待进入呼叫

SYN_RECV  //一个连接请求已经到达，等待确认

SYN_SENT  //应用已经开始，打开一个连接

ESTABLISHED  //正常数据传输状态/当前并发连接数

FIN_WAIT1  //应用说它已经完成

FIN_WAIT2  //另一边已同意释放

ITMED_WAIT  //等待所有分组死掉

CLOSING  //两边同时尝试关闭

TIME_WAIT  //另一边已初始化一个释放

LAST_ACK  //等待所有分组死掉


type--subtype
1--主机
    AIX(10001),HP(10002),Linux(10003),Win(10004)
2--存储
3--数据库
    Oracle(20001),MSSQL(20002),MySQL(20003)
phyposition
1--IBM-740
2--IBM-750
3--VMWARE
4--刀片-A
5--HP-A
6--HP-B
7--阿里

zoneid
1--POS
2--OA
3--BI
4--PORTAL BPM SOA
5--供应链
6--微信
7--老DRP
8--ERP
9--VIP会员
10--网站
11--HR
15--其他

drop table mn_itbase_mapping;
create table mn_itbase_mapping(
type varchar2(100),
name varchar2(100),
value number
)tablespace mn;
create unique index mnitbasemappingind on mn_itbase_mapping(type,value);
insert into mn_itbase_mapping values('type','主机',1);
insert into mn_itbase_mapping values('type','存储',2);
insert into mn_itbase_mapping values('type','数据库',3);
insert into mn_itbase_mapping values('subtype','AIX',10001);
insert into mn_itbase_mapping values('subtype','HP',10002);
insert into mn_itbase_mapping values('subtype','Linux',10003);
insert into mn_itbase_mapping values('subtype','Win',10004);
insert into mn_itbase_mapping values('subtype','Oracle',20001);
insert into mn_itbase_mapping values('subtype','MSSQL',20002);
insert into mn_itbase_mapping values('subtype','MySQL',20003); 
insert into mn_itbase_mapping values('phyposition','IBM-740',1);  
insert into mn_itbase_mapping values('phyposition','IBM-750',2);  
insert into mn_itbase_mapping values('phyposition','VMWARE',3);  
insert into mn_itbase_mapping values('phyposition','刀片',4); 
insert into mn_itbase_mapping values('phyposition','HP-A',5);   
insert into mn_itbase_mapping values('phyposition','HP-B',6);   
insert into mn_itbase_mapping values('phyposition','阿里',7);     
insert into mn_itbase_mapping values('zone','POS',1);  
insert into mn_itbase_mapping values('zone','OA',2);  
insert into mn_itbase_mapping values('zone','BI',3);  
insert into mn_itbase_mapping values('zone','PORTAL BPM SOA',4);  
insert into mn_itbase_mapping values('zone','供应链',5);  
insert into mn_itbase_mapping values('zone','微信',6);  
insert into mn_itbase_mapping values('zone','老DRP',7);  
insert into mn_itbase_mapping values('zone','ERP',8);   
insert into mn_itbase_mapping values('zone','VIP会员',9);  
insert into mn_itbase_mapping values('zone','网站',10);  
insert into mn_itbase_mapping values('zone','HR',11);  
insert into mn_itbase_mapping values('zone','其他',15);   

select a.*,
(select name from mn_itbase_mapping i where i.value=a.zoneid and i.type='zone') zonename,
(select name from mn_itbase_mapping i where i.value=a.phypositionid and i.type='phyposition') phypositionname,
(select name from mn_itbase_mapping i where i.value=a.typeid and i.type='type') typename,
(select name from mn_itbase_mapping i where i.value=a.subtypeid and i.type='subtype') subtypename
from mn_itbase a

drop table mn_itbase; 
create table mn_itbase
(
id number,
name varchar2(500),
zoneid number not null, 
typeid number not null,
subtypeid number not null,
phypositionid number not null,
status varchar2(100) not null,   
prod varchar2(100) not null,   --生产|测试|开发
mark varchar2(1000),
A varchar2(500),  --unqiue
B varchar2(500),
C varchar2(500),
D varchar2(500),
E varchar2(500),
F varchar2(500),
G varchar2(500),
H varchar2(500),
udate date
)tablespace mn;
create unique index mnitbaseind1 on mn_itbase(id) tablespace mn;
create index mnitbaseind2 on mn_itbase(typeid) tablespace mn;
create index mnitbaseind3 on mn_itbase(subtypeid) tablespace mn;
create index mnitbaseind4 on mn_itbase(phypositionid) tablespace mn; 
create index mnitbaseind5 on mn_itbase(zoneid) tablespace mn; 
create unique index mnitbaseind6 on mn_itbase(phypositionid,typeid,subtypeid,name,a) tablespace mn;

select * from mn_itbase where zoneid=1

按照硬件分类
select a.* from mn_itbase a where phypositionid=3
按照业务分类

zoneid:
zonename
datalist:[]
http://127.0.0.1:89/data/inFraDataOper?opertype=querybyZone