####################执行计划#####################
Crontab:
################# Mn Server ##################
##autoconf node
5 5 * * *                sh /mn/sbin/mn/mnauto.sh >/dev/null
##email check
1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,51,53,55,57,59 * * * * sh /mn/plugin/appwchk/wchk.sh>/dev/null
##if agent upload use ftp
1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,51,53,55,57,59  * * * * sh /mn/sbin/mn/mnupload.sh >/dev/null
################# Mn Client ##################

http://127.0.0.1:8888/dashboard/warnrun


1,15,20,25,30,35,40,45,50,55  * * * * wget http://192.168.188.209/dashboard/warnrun  

###################目录结构说明###################
bin
	--oracle		--Oracle相关调用
	--os			--OS相关调用
	--node          --本节点相关内容
sbin
	--mn			--mn相关执行文件
	--util			--ODS tool工具
lib					--公共文件
	--jlibf			--java相关公共文件
	--sysf			--shell sys相关公共文件
conf				
	mn.cnf			--Mn主要配置文件    
install				--安装初始化
plugin				--(非必须,运行脚本自动创建)
locks				--(非必须,运行脚本自动创建)
						说明:lock锁存放
							once类型
							*.lck.once 
							Mnbase
							*.pid	
							其他
							*.lck
data				--(非必须,运行脚本自动创建)
tmp					--(非必须,运行脚本自动创建)

#调用顺序
Mn.sh-->nohup Mnbase.sh &-->run.sh


node字段
host
host.db.{oracle_sid}
host.app.{tomcate} 


#修改记录
2015-03-27
1>修改load.sh的bug,主机新开机无法获得load
1>修改hostinfo.sh,将数据添加到mn_node_info_dtl表中

2015-03-26
1>修改fs.sh的bug,无法导入第一行数据
 
2015-03-24
1>重写Mn.sh脚本,以及将rid设置为伪随机修改相应的表结构
2>添加m.sh脚本,菜单的方式执行

2015-03-15
1>添加oracle/db/topcpu.sh功能

2015-03-12
1>添加sbin/mnauto.sh 维护数据库中的表

2015-03-11
1>添加db/onoff.sh 数据库为读写且lsnr运行的时候,返回数据库on

2015-03-09
1>添加hostinfo.sh脚本,删除nodeinfo.sh脚本

2015-03-08
1>修改Mnbase代码,将Mnbase.lck改成Mnbase.pid
2>修改Mn.sh代码,重写stop,start部分

2015-02-03
1>nodeinfo暂时去除,不可用

2014-05-07
1>重新编写了wchk.jar发送邮件的包s

2014-05-04
1>修改Mncrontab实际无法执行的bug,删除cronreclog,将日志重定向到reclog函数

2014-05-02
1>添加./lib/sysf/os_changedate.fun,计算当前日期的前后N天日期,纯shell实现
2>优化日志输出代码

2014-04-30
1>添加日志输出${scriptsdir}/tmp/alertmn.log

2014-04-25
1>添加rac,crs_statdtl.sh功能

2014-04-23
1>添加MnCron功能,防重启中断[选择]

2014-04-11
1>添加新的上传方式,agent数据通过ftp上传,用于解决与MN server网络不通时使用

2014-04-08
1>添加util.oragetsid,util.oragetpid功能
	ln -s /mn/sbin/util/oradb/oragetpid  /mn/sbin/oragetpid
	ln -s /mn/sbin/util/oradb/oragetsid  /mn/sbin/oragetsid

2014-04-06
1>修改Mnbase代码
2>shell_list 添加注释功能
3>添加nodeautoconf功能
4>添加nodedel功能
5>优化脚本细节

2014-04-05
1>移除非公共代码,重新组织

2014-03-31
1>移除localsave 变量,去除本地保存功能
2>Mnbase.sh中添加心跳设置

