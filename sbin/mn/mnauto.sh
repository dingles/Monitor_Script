#!/bin/sh
#mn.cnf:user,pwd
scriptsdir=$(cd `dirname $0`; pwd)/../..
. ${scriptsdir}/conf/mn.cnf
. ${scriptsdir}/lib/sysf/ora_env.fun
. ${scriptsdir}/lib/sysf/reclog.fun

if [[ -n $1 ]];then
	export ORACLE_SID=$1
else
	echo "No SID"
	exit 1
fi
reclog 'MNAUTO' ' Mnauto run.'
#table need to analyze list
analist="${scriptsdir}/tmp/anatablist.sql"
>$analist
tablelist='
mn_os_netflow
mn_os_fs
mn_os_cpumem
mn_os_load
mn_oradb_archsw
mn_oradb_size
mn_oradb_tbs
mn_oradb_session
mn_oradb_ratio
'
for tab in $tablelist
do
	echo "--analyze table $tab compute statistics;">>$analist
	echo "analyze table $tab compute statistics;">>$analist
	echo "--delete from $tab where inserttime<sysdate-360;">>$analist
	echo "delete from $tab where inserttime<sysdate-360;">>$analist
done

#process mn_node_heartbeat and table analyze
sqlplus $user/$pwd <<EOF
--clear tab mn_node_heartbeat 
delete from mn_node_heartbeat where (node,updatetime) in (select node,updatetime from (select node,updatetime,row_number() over(partition by node order by updatetime desc) rid from mn_node_heartbeat)where rid>100);
commit;
@$analist
commit;
quit
EOF

